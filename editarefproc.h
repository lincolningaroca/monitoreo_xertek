#pragma once

#include <QDialog>
#include <bussineslayer.h>

namespace Ui {
class EditaRefProc;
}

class EditaRefProc : public QDialog {
  Q_OBJECT

public:
  explicit EditaRefProc(unsigned int op, QVariantList value, QWidget *parent = nullptr);
  ~EditaRefProc();

private slots:
  void on_btnCancelar_clicked();
  void on_btnGuardar_clicked();

private:
  Ui::EditaRefProc *ui;
  const unsigned int m_op{1};
  const QVariantList m_value{};
  BussinesLayer m_bLayer;
};
