#pragma once

#include <QDialog>

namespace Ui {
class FotoDialog;
}

class FotoDialog : public QDialog {
  Q_OBJECT

public:
  explicit FotoDialog(QStringView title, QImage foto, QWidget *parent = nullptr);

  ~FotoDialog();

private:
  Ui::FotoDialog *ui;
  const QStringView m_tittle{};
  const QImage m_foto{};
  void loadFoto();
};
