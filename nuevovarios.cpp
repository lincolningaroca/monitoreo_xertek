#include "nuevovarios.h"
#include "ui_nuevovarios.h"
#include <QMessageBox>

NuevoVarios::NuevoVarios(QWidget *parent) :
  QDialog(parent), ui(new Ui::NuevoVarios)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  loadDataClient();
}

NuevoVarios::~NuevoVarios(){ delete ui;}

bool NuevoVarios::insertTablaMonitoreo()
{
  if(ui->txtProcedencia->toPlainText().simplified().isEmpty()){
    QMessageBox::warning(this,qApp->applicationName(),"Ingrese una procedencia por favor.");
    ui->txtProcedencia->setFocus();
    return false;
  }
  QVariantList param{};
  param.append(ui->txtProcedencia->toPlainText().toUpper());
  param.append(m_dataClient.key(ui->cboCliente->currentText()));
  if(!m_bLayer.tablaMonitoreo(param,BussinesLayer::TypeStm::INSERT)){
    if(m_bLayer.errorCode()=="23505"){
      QMessageBox::warning(this,qApp->applicationName(), "Ya existe una procedencia con este nombre:\n"+
                            ui->txtProcedencia->toPlainText().prepend("\"").append("\""));
      ui->txtProcedencia->selectAll();
      ui->txtProcedencia->setFocus(Qt::OtherFocusReason);
      return false;
    }
    QMessageBox::critical(this,qApp->applicationName(),"Error al guardar los datos.\n"+
                          m_bLayer.errorMessage());
    return false;
  }

  return true;

}

void NuevoVarios::loadDataClient()
{
  m_dataClient=m_bLayer.selectDataClient();
  ui->cboCliente->addItems(m_dataClient.values());

}

void NuevoVarios::on_btnAceptar_clicked()
{
  if(!insertTablaMonitoreo()){
    return;
  }
  QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
  accept();
}


void NuevoVarios::on_btnCancelar_clicked()
{
  reject();
}

