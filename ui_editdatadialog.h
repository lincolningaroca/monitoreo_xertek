/********************************************************************************
** Form generated from reading UI file 'editdatadialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITDATADIALOG_H
#define UI_EDITDATADIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <swcustomtxt.h>

QT_BEGIN_NAMESPACE

class Ui_EditDataDialog
{
public:
    QGridLayout *gridLayout_6;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *cboGrupo;
    QLabel *label_2;
    QComboBox *cboUnidad;
    QLabel *label_17;
    QListWidget *lwProcedencia;
    QLabel *label_16;
    QComboBox *cboReferencia;
    QTableView *twEstaciones;
    QFormLayout *formLayout;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QDateEdit *dateEdit;
    QLabel *label_5;
    QTimeEdit *timeEdit;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QLabel *label_6;
    SWCustomTxt *lineEdit_2;
    QToolButton *toolButton;
    QLabel *label_7;
    SWCustomTxt *lineEdit_3;
    QToolButton *toolButton_2;
    QLabel *label_8;
    SWCustomTxt *lineEdit_4;
    QToolButton *toolButton_3;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QFormLayout *formLayout_3;
    QLabel *label_9;
    QDoubleSpinBox *dsbPh;
    QFormLayout *formLayout_4;
    QLabel *label_11;
    QDoubleSpinBox *dsbTemp;
    QFormLayout *formLayout_5;
    QLabel *label_10;
    QDoubleSpinBox *dsbOd;
    QFormLayout *formLayout_6;
    QLabel *label_12;
    QDoubleSpinBox *dsbCond;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_5;
    QLabel *label_13;
    QDoubleSpinBox *dsbEste;
    QLabel *label_14;
    QDoubleSpinBox *dsbNorte;
    QLabel *label_15;
    QDoubleSpinBox *dsbCota;
    QPushButton *pushButton;
    QGridLayout *gridLayout_2;
    QLabel *label_18;
    QComboBox *cboMatriz;
    QLabel *lblMatriz;
    QFormLayout *formLayout_2;
    QLabel *label_20;
    QComboBox *cboNorma;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btnGuardar;
    QPushButton *btnEliminar;
    QFrame *line;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *btnEditaProc;
    QToolButton *btnEditaref;
    QPushButton *btnCancelar;

    void setupUi(QDialog *EditDataDialog)
    {
        if (EditDataDialog->objectName().isEmpty())
            EditDataDialog->setObjectName(QString::fromUtf8("EditDataDialog"));
        EditDataDialog->resize(809, 598);
        gridLayout_6 = new QGridLayout(EditDataDialog);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(EditDataDialog);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        cboGrupo = new QComboBox(EditDataDialog);
        cboGrupo->setObjectName(QString::fromUtf8("cboGrupo"));

        gridLayout->addWidget(cboGrupo, 0, 1, 1, 1);

        label_2 = new QLabel(EditDataDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        cboUnidad = new QComboBox(EditDataDialog);
        cboUnidad->setObjectName(QString::fromUtf8("cboUnidad"));

        gridLayout->addWidget(cboUnidad, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        label_17 = new QLabel(EditDataDialog);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        verticalLayout->addWidget(label_17);

        lwProcedencia = new QListWidget(EditDataDialog);
        lwProcedencia->setObjectName(QString::fromUtf8("lwProcedencia"));

        verticalLayout->addWidget(lwProcedencia);

        label_16 = new QLabel(EditDataDialog);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        verticalLayout->addWidget(label_16);

        cboReferencia = new QComboBox(EditDataDialog);
        cboReferencia->setObjectName(QString::fromUtf8("cboReferencia"));

        verticalLayout->addWidget(cboReferencia);

        twEstaciones = new QTableView(EditDataDialog);
        twEstaciones->setObjectName(QString::fromUtf8("twEstaciones"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(twEstaciones->sizePolicy().hasHeightForWidth());
        twEstaciones->setSizePolicy(sizePolicy);
        twEstaciones->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout->addWidget(twEstaciones);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_3 = new QLabel(EditDataDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_3);

        lineEdit = new QLineEdit(EditDataDialog);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit);

        label_4 = new QLabel(EditDataDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_4);

        dateEdit = new QDateEdit(EditDataDialog);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
        dateEdit->setCalendarPopup(true);

        formLayout->setWidget(1, QFormLayout::FieldRole, dateEdit);

        label_5 = new QLabel(EditDataDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        timeEdit = new QTimeEdit(EditDataDialog);
        timeEdit->setObjectName(QString::fromUtf8("timeEdit"));

        formLayout->setWidget(2, QFormLayout::FieldRole, timeEdit);


        verticalLayout->addLayout(formLayout);


        gridLayout_6->addLayout(verticalLayout, 0, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_2 = new QGroupBox(EditDataDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_3->addWidget(label_6, 0, 0, 1, 1);

        lineEdit_2 = new SWCustomTxt(groupBox_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setReadOnly(true);

        gridLayout_3->addWidget(lineEdit_2, 0, 1, 1, 1);

        toolButton = new QToolButton(groupBox_2);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));

        gridLayout_3->addWidget(toolButton, 0, 2, 1, 1);

        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_3->addWidget(label_7, 1, 0, 1, 1);

        lineEdit_3 = new SWCustomTxt(groupBox_2);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setReadOnly(true);

        gridLayout_3->addWidget(lineEdit_3, 1, 1, 1, 1);

        toolButton_2 = new QToolButton(groupBox_2);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));

        gridLayout_3->addWidget(toolButton_2, 1, 2, 1, 1);

        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_3->addWidget(label_8, 2, 0, 1, 1);

        lineEdit_4 = new SWCustomTxt(groupBox_2);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setReadOnly(true);

        gridLayout_3->addWidget(lineEdit_4, 2, 1, 1, 1);

        toolButton_3 = new QToolButton(groupBox_2);
        toolButton_3->setObjectName(QString::fromUtf8("toolButton_3"));

        gridLayout_3->addWidget(toolButton_3, 2, 2, 1, 1);


        verticalLayout_2->addWidget(groupBox_2);

        groupBox_4 = new QGroupBox(EditDataDialog);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        sizePolicy1.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy1);
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_9);

        dsbPh = new QDoubleSpinBox(groupBox_4);
        dsbPh->setObjectName(QString::fromUtf8("dsbPh"));
        dsbPh->setMinimumSize(QSize(133, 0));
        dsbPh->setMaximumSize(QSize(133, 16777215));
        dsbPh->setReadOnly(false);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, dsbPh);


        gridLayout_4->addLayout(formLayout_3, 0, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_11);

        dsbTemp = new QDoubleSpinBox(groupBox_4);
        dsbTemp->setObjectName(QString::fromUtf8("dsbTemp"));
        dsbTemp->setMinimumSize(QSize(133, 0));
        dsbTemp->setMaximumSize(QSize(133, 16777215));
        dsbTemp->setReadOnly(false);
        dsbTemp->setMaximum(999.990000000000009);

        formLayout_4->setWidget(0, QFormLayout::FieldRole, dsbTemp);


        gridLayout_4->addLayout(formLayout_4, 0, 1, 1, 1);

        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_10);

        dsbOd = new QDoubleSpinBox(groupBox_4);
        dsbOd->setObjectName(QString::fromUtf8("dsbOd"));
        dsbOd->setMinimumSize(QSize(133, 0));
        dsbOd->setMaximumSize(QSize(133, 16777215));
        dsbOd->setReadOnly(false);

        formLayout_5->setWidget(0, QFormLayout::FieldRole, dsbOd);


        gridLayout_4->addLayout(formLayout_5, 1, 0, 1, 1);

        formLayout_6 = new QFormLayout();
        formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_12);

        dsbCond = new QDoubleSpinBox(groupBox_4);
        dsbCond->setObjectName(QString::fromUtf8("dsbCond"));
        dsbCond->setMinimumSize(QSize(133, 0));
        dsbCond->setMaximumSize(QSize(133, 16777215));
        dsbCond->setReadOnly(false);
        dsbCond->setMaximum(999999999.990000009536743);

        formLayout_6->setWidget(0, QFormLayout::FieldRole, dsbCond);


        gridLayout_4->addLayout(formLayout_6, 1, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(EditDataDialog);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        sizePolicy1.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy1);
        gridLayout_5 = new QGridLayout(groupBox_5);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_13 = new QLabel(groupBox_5);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_5->addWidget(label_13, 0, 0, 1, 1);

        dsbEste = new QDoubleSpinBox(groupBox_5);
        dsbEste->setObjectName(QString::fromUtf8("dsbEste"));
        dsbEste->setMinimumSize(QSize(120, 0));
        dsbEste->setMaximumSize(QSize(120, 16777215));
        dsbEste->setMaximum(9999999999.000000000000000);

        gridLayout_5->addWidget(dsbEste, 0, 1, 1, 1);

        label_14 = new QLabel(groupBox_5);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_5->addWidget(label_14, 0, 2, 1, 1);

        dsbNorte = new QDoubleSpinBox(groupBox_5);
        dsbNorte->setObjectName(QString::fromUtf8("dsbNorte"));
        dsbNorte->setMinimumSize(QSize(120, 0));
        dsbNorte->setMaximumSize(QSize(120, 16777215));
        dsbNorte->setMaximum(9999999999.000000000000000);

        gridLayout_5->addWidget(dsbNorte, 0, 3, 1, 1);

        label_15 = new QLabel(groupBox_5);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_5->addWidget(label_15, 1, 0, 1, 1);

        dsbCota = new QDoubleSpinBox(groupBox_5);
        dsbCota->setObjectName(QString::fromUtf8("dsbCota"));
        dsbCota->setMinimumSize(QSize(150, 0));
        dsbCota->setMaximumSize(QSize(150, 16777215));
        dsbCota->setMaximum(9999999999.000000000000000);

        gridLayout_5->addWidget(dsbCota, 1, 1, 1, 1);

        pushButton = new QPushButton(groupBox_5);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/ui_-09-128.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon);

        gridLayout_5->addWidget(pushButton, 1, 3, 1, 1);


        verticalLayout_2->addWidget(groupBox_5);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_18 = new QLabel(EditDataDialog);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_2->addWidget(label_18, 0, 0, 1, 1);

        cboMatriz = new QComboBox(EditDataDialog);
        cboMatriz->setObjectName(QString::fromUtf8("cboMatriz"));

        gridLayout_2->addWidget(cboMatriz, 0, 1, 1, 1);

        lblMatriz = new QLabel(EditDataDialog);
        lblMatriz->setObjectName(QString::fromUtf8("lblMatriz"));
        lblMatriz->setFrameShape(QFrame::Box);
        lblMatriz->setFrameShadow(QFrame::Raised);

        gridLayout_2->addWidget(lblMatriz, 1, 0, 1, 2);


        verticalLayout_2->addLayout(gridLayout_2);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_20 = new QLabel(EditDataDialog);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_20);

        cboNorma = new QComboBox(EditDataDialog);
        cboNorma->setObjectName(QString::fromUtf8("cboNorma"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, cboNorma);


        verticalLayout_2->addLayout(formLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(210, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        btnGuardar = new QPushButton(EditDataDialog);
        btnGuardar->setObjectName(QString::fromUtf8("btnGuardar"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/img/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnGuardar->setIcon(icon1);
        btnGuardar->setIconSize(QSize(24, 24));

        horizontalLayout_3->addWidget(btnGuardar);

        btnEliminar = new QPushButton(EditDataDialog);
        btnEliminar->setObjectName(QString::fromUtf8("btnEliminar"));
        btnEliminar->setMinimumSize(QSize(0, 0));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/img/807714.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnEliminar->setIcon(icon2);
        btnEliminar->setIconSize(QSize(24, 24));

        horizontalLayout_3->addWidget(btnEliminar);


        verticalLayout_2->addLayout(horizontalLayout_3);

        line = new QFrame(EditDataDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);


        gridLayout_6->addLayout(verticalLayout_2, 0, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));

        btnEditaProc=new QToolButton(EditDataDialog);
        btnEditaProc->setObjectName(QString::fromUtf8("btnEditaProc"));
        btnEditaProc->setText("...");
        btnEditaProc->setMinimumHeight(24);
        btnEditaProc->setToolTip("Editar el nombre de la procedencia");
        horizontalLayout_2->addWidget(btnEditaProc);

        btnEditaref=new QToolButton(EditDataDialog);
        btnEditaref->setObjectName(QString::fromUtf8("btnEditaRef"));
        btnEditaref->setText("...");
        btnEditaref->setMinimumHeight(24);
        btnEditaref->setToolTip("Editar el nombre de la referencia");
        horizontalLayout_2->addWidget(btnEditaref);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);




        horizontalLayout_2->addItem(horizontalSpacer_2);



        btnCancelar = new QPushButton(EditDataDialog);
        btnCancelar->setObjectName(QString::fromUtf8("btnCancelar"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/img/door-out-128.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnCancelar->setIcon(icon3);
        btnCancelar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnCancelar);


        gridLayout_6->addLayout(horizontalLayout_2, 1, 0, 1, 2);


        retranslateUi(EditDataDialog);

        QMetaObject::connectSlotsByName(EditDataDialog);
    } // setupUi

    void retranslateUi(QDialog *EditDataDialog)
    {
        EditDataDialog->setWindowTitle(QCoreApplication::translate("EditDataDialog", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("EditDataDialog", "Grupo o compa\303\261\303\255a minera:", nullptr));
        label_2->setText(QCoreApplication::translate("EditDataDialog", "Unidad minera:", nullptr));
        label_17->setText(QCoreApplication::translate("EditDataDialog", "Procedencia:", nullptr));
        label_16->setText(QCoreApplication::translate("EditDataDialog", "Referencia:", nullptr));
        label_3->setText(QCoreApplication::translate("EditDataDialog", "C\303\263digo de estaci\303\263n:", nullptr));
        label_4->setText(QCoreApplication::translate("EditDataDialog", "Fecha:", nullptr));
        label_5->setText(QCoreApplication::translate("EditDataDialog", "Hora:", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("EditDataDialog", "Fotos", nullptr));
        label_6->setText(QCoreApplication::translate("EditDataDialog", "Foto 1:", nullptr));
#if QT_CONFIG(tooltip)
        lineEdit_2->setToolTip(QCoreApplication::translate("EditDataDialog", "Click para ver la foto", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        toolButton->setToolTip(QCoreApplication::translate("EditDataDialog", "Cargar primera imagen (F1)", nullptr));
#endif // QT_CONFIG(tooltip)
        toolButton->setText(QCoreApplication::translate("EditDataDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        toolButton->setShortcut(QCoreApplication::translate("EditDataDialog", "F1", nullptr));
#endif // QT_CONFIG(shortcut)
        label_7->setText(QCoreApplication::translate("EditDataDialog", "Foto 2:", nullptr));
#if QT_CONFIG(tooltip)
        lineEdit_3->setToolTip(QCoreApplication::translate("EditDataDialog", "Click para ver la foto", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        toolButton_2->setToolTip(QCoreApplication::translate("EditDataDialog", "Cargar segunda imagen (F2)", nullptr));
#endif // QT_CONFIG(tooltip)
        toolButton_2->setText(QCoreApplication::translate("EditDataDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        toolButton_2->setShortcut(QCoreApplication::translate("EditDataDialog", "F2", nullptr));
#endif // QT_CONFIG(shortcut)
        label_8->setText(QCoreApplication::translate("EditDataDialog", "Foto 3:", nullptr));
#if QT_CONFIG(tooltip)
        lineEdit_4->setToolTip(QCoreApplication::translate("EditDataDialog", "Click para ver la foto", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        toolButton_3->setToolTip(QCoreApplication::translate("EditDataDialog", "Cargar tercera imagen (F3)", nullptr));
#endif // QT_CONFIG(tooltip)
        toolButton_3->setText(QCoreApplication::translate("EditDataDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        toolButton_3->setShortcut(QCoreApplication::translate("EditDataDialog", "F3", nullptr));
#endif // QT_CONFIG(shortcut)
        groupBox_4->setTitle(QCoreApplication::translate("EditDataDialog", "Par\303\241metros de campo", nullptr));
        label_9->setText(QCoreApplication::translate("EditDataDialog", "PH:", nullptr));
        label_11->setText(QCoreApplication::translate("EditDataDialog", "Temp:", nullptr));
        label_10->setText(QCoreApplication::translate("EditDataDialog", "OD:", nullptr));
        label_12->setText(QCoreApplication::translate("EditDataDialog", "Cond:", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("EditDataDialog", "Coordenadas", nullptr));
        label_13->setText(QCoreApplication::translate("EditDataDialog", "Este:", nullptr));
        label_14->setText(QCoreApplication::translate("EditDataDialog", "Norte:", nullptr));
        label_15->setText(QCoreApplication::translate("EditDataDialog", "Cota:", nullptr));
        dsbCota->setSuffix(QCoreApplication::translate("EditDataDialog", "  (m.s.n.m)", nullptr));
#if QT_CONFIG(tooltip)
        pushButton->setToolTip(QCoreApplication::translate("EditDataDialog", "(Ctrl+D) Para abrir la ventana", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton->setText(QCoreApplication::translate("EditDataDialog", "Descripci\303\263n del punto", nullptr));
#if QT_CONFIG(shortcut)
        pushButton->setShortcut(QCoreApplication::translate("EditDataDialog", "Ctrl+D", nullptr));
#endif // QT_CONFIG(shortcut)
        label_18->setText(QCoreApplication::translate("EditDataDialog", "Matriz:", nullptr));
        lblMatriz->setText(QString());
        label_20->setText(QCoreApplication::translate("EditDataDialog", "Norma de comparaci\303\263n:", nullptr));
#if QT_CONFIG(tooltip)
        btnGuardar->setToolTip(QCoreApplication::translate("EditDataDialog", "(Ctrl+S) Para guardar los cambios", nullptr));
#endif // QT_CONFIG(tooltip)
        btnGuardar->setText(QCoreApplication::translate("EditDataDialog", "Guardar cambios", nullptr));
#if QT_CONFIG(shortcut)
        btnGuardar->setShortcut(QCoreApplication::translate("EditDataDialog", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnEliminar->setToolTip(QCoreApplication::translate("EditDataDialog", "(Ctrl+E) Para Eliminar", nullptr));
#endif // QT_CONFIG(tooltip)
        btnEliminar->setText(QCoreApplication::translate("EditDataDialog", "Eliminar", nullptr));
#if QT_CONFIG(shortcut)
        btnEliminar->setShortcut(QCoreApplication::translate("EditDataDialog", "Ctrl+E", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnCancelar->setToolTip(QCoreApplication::translate("EditDataDialog", "(Esc) Para salir", nullptr));
#endif // QT_CONFIG(tooltip)
        btnCancelar->setText(QCoreApplication::translate("EditDataDialog", "Salir", nullptr));
#if QT_CONFIG(shortcut)
        btnCancelar->setShortcut(QCoreApplication::translate("EditDataDialog", "Esc", nullptr));
#endif // QT_CONFIG(shortcut)
    } // retranslateUi

};

namespace Ui {
    class EditDataDialog: public Ui_EditDataDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITDATADIALOG_H
