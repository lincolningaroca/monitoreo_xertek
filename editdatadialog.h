#pragma once

#include <QDialog>
#include "bussineslayer.h"

namespace Ui {
class EditDataDialog;

}

class EditDataDialog : public QDialog {
  Q_OBJECT

public:
  explicit EditDataDialog(QWidget *parent = nullptr);
  ~EditDataDialog();

  // funciones del programador
  void loadGpoMinerolist() noexcept;
  void loadDataListCliente() noexcept;
  void datosMonitoreo() noexcept;

private slots:
  //  void on_txtPath1_returnPressed();
  void on_btnCancelar_clicked();
  void on_cboGrupo_activated(int index);
  void on_cboUnidad_activated(int index);
  void on_btnEliminar_clicked();
  void on_btnGuardar_clicked();
  void on_toolButton_clicked();
  void on_pushButton_clicked();
  void on_toolButton_2_clicked();
  void on_toolButton_3_clicked();
  void on_lwProcedencia_itemSelectionChanged();
  void on_cboReferencia_activated(int index);
  void on_cboMatriz_activated(const QString &arg1);

private:
  Ui::EditDataDialog *ui;
  BussinesLayer m_bLayer{};
  QHash<unsigned int, QString> m_dataList{};
  QHash<unsigned int, QString> m_dataListCliente{};
  QHash<unsigned int, QString> m_dataTableProc{};
  QHash<unsigned int, QString> m_dataTableRef{};
  QHash<QString, QString> m_matrizList{};
  QSqlQueryModel *m_model{nullptr};
  QVariantList m_list{};
  QString m_desc_punto{};

  QString m_newPath_1{};
  QString m_newPath_2{};
  QString m_newPath_3{};

  QString m_pathImgOriginal_1{};
  QString m_pathImgOriginal_2{};
  QString m_pathImgOriginal_3{};

  QImage m_imagen_1{};
  QImage m_imagen_2{};
  QImage m_imagen_3{};

  QString m_pathToImage{};

  void dataModel() noexcept;
  //  void cleanData();
  void manageControls(const int &op) noexcept;
  QImage openPicture(const QString &f) const noexcept;
  //  QImage removePicture(QString f);
  // funcoines privadas del programador
  // Metodos para caragar y escribir las configuraciones en el registro.
  void writeSettings() const noexcept;
  void readSettings() noexcept;

  void setUpTableView() noexcept;
  void setUpToolBtnClear() noexcept;
  void setupFotoDialog() noexcept;
  void tableProcedencia() noexcept;
  void tableReferencia() noexcept;
  void fillCboMatriz() noexcept;
  void fillCboNComparacion() noexcept;

  QString showDialogFile() noexcept;
  bool fileExists(const QString &fName) noexcept;
  void setPath(QString &fileName, QString& nPath, QString &oPath) noexcept;
  // QPaintDevice interface
public:
  virtual QPaintEngine *paintEngine() const override;

  // QWidget interface
protected:
  virtual void closeEvent(QCloseEvent *event) override;
};


