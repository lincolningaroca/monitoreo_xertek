#include "desc_pdialog.h"
#include "ui_Desc_pdialog.h"
#include <QDialog>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDebug>

Desc_pDialog::Desc_pDialog(unsigned int mode, QString descr, QWidget *parent)
  : QDialog(parent), ui(new Ui::Desc_pDialog), m_d(descr), m_mo(mode) {
  ui->setupUi(this);

  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  if (m_mo == 1) {
      ui->txtDescPunto->setPlainText(m_d);
      ui->txtDescPunto->setReadOnly(true);

    } else {
      ui->txtDescPunto->setPlainText(m_d);
      //    ui->txtDescPunto->clear();
      ui->txtDescPunto->setReadOnly(false);
      m_btnGuardar = new QPushButton("Aceptar", this);
      m_btnGuardar->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
      m_btnCancelar = new QPushButton("Cancelar", this);
      QHBoxLayout *hLayOut = new QHBoxLayout{nullptr};

      hLayOut->addStretch();
      hLayOut->addWidget(m_btnGuardar);
      hLayOut->addWidget(m_btnCancelar);
//      dynamic_cast<QVBoxLayout *>(this->layout())->addLayout(hLayOut);
      this->layout()->addItem(hLayOut);
      //    this->layout()->addItem(hLayOut);

      QObject::connect(m_btnGuardar, &QPushButton::clicked, this,[&]() {
          m_desc = ui->txtDescPunto->toPlainText();
          accept();
        });
      QObject::connect(m_btnCancelar, &QPushButton::clicked, this, &Desc_pDialog::reject);

    }

}

Desc_pDialog::Desc_pDialog(QString descr, QWidget *parent)
  : QDialog(parent) , m_d(descr){

  ui->txtDescPunto->setPlainText(m_d);
}

Desc_pDialog::~Desc_pDialog() { delete ui;}
