#pragma once

#include <QString>
class QSqlDatabase;

class DbConection {
public:
  explicit DbConection()=default;
  bool connect(const QString &dbName);
  QString errorMessage() const noexcept { return m_errorMessage; }
  QSqlDatabase getDataBase() const noexcept;
  void closeCn();

private:
  QString m_errorMessage{};
  QString m_dbName{};
};
