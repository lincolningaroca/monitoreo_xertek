#pragma once

#include <QDialog>

namespace Ui {
  class frmAcercaDe;
}

class frmAcercaDe : public QDialog {
  Q_OBJECT

public:
  explicit frmAcercaDe(unsigned int mode, QWidget *parent = nullptr);
  ~frmAcercaDe();

private slots:
  void on_toolButton_clicked();
  void on_btnAceptar_clicked();

private:
  Ui::frmAcercaDe *ui;
  const unsigned int m_mode{0};
  void loadLicencia() noexcept;
};
