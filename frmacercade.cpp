#include "frmacercade.h"
#include "ui_frmacercade.h"
#include <QDebug>
#include <QDesktopServices>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

frmAcercaDe::frmAcercaDe(unsigned int mode, QWidget *parent)
  : QDialog(parent), ui(new Ui::frmAcercaDe), m_mode(mode) {

  ui->setupUi(this);

  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  setFixedSize(649, 433);

  ui->tabWidget->setCurrentIndex(0);
  if (m_mode == 0) {
      ui->lblLogo->setPixmap(QPixmap(":/img/logoEmpresa.png"));
    } else {
      ui->lblLogo->setPixmap(QPixmap(":/img/logoEmpresa_1.png"));
    }

  loadLicencia();
  ui->lblLicencia->setText("<a href='message'>Ver licencia.</a>");

  connect(ui->lblLicencia, &QLabel::linkActivated, this, [&]() {
      QDialog licenciDlg(this);

      QScopedPointer<QVBoxLayout> mainLayOut(new QVBoxLayout(&licenciDlg));
      QScopedPointer<QTextEdit> teLicencia(new QTextEdit(&licenciDlg));

      QFile fileName(":/licencia/licencia.txt");
      if (!fileName.open(QFile::ReadOnly | QFile::Text)) {
          QMessageBox::warning(nullptr, qApp->applicationName(), "Error al abrir el archivo.\n" +
                               fileName.errorString());
          return;
        }

      teLicencia->setFontPointSize(10.1);
      teLicencia->setPlainText(fileName.readAll());
      teLicencia->setReadOnly(true);
      mainLayOut->addWidget(teLicencia.data());
      licenciDlg.setLayout(mainLayOut.data());
      licenciDlg.setFixedSize(600, 477);
      licenciDlg.exec();
    });
}

frmAcercaDe::~frmAcercaDe() { delete ui;}

void frmAcercaDe::on_toolButton_clicked() {
  QMessageBox::aboutQt(this, "Acerca de Qt!");
}

void frmAcercaDe::on_btnAceptar_clicked() { accept(); }

void frmAcercaDe::loadLicencia() noexcept{
  ui->textBrowser->setAcceptRichText(true);
  ui->textBrowser->setOpenExternalLinks(true);
  ui->textBrowser->setHtml(
        "<p><font size=\"4\">SWDatos_monitoreo:<br><br>Es software libre, puede "
        "redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública "
        "General de GNU según se encuentra publicada por la <a href=\"https://www.fsf.org\">Free Software "
        "Foundation</a>, bien de la versión 3 de dicha Licencia o bien (según su "
        "elección) de cualquier versión posterior.<br><br>"
        "Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA "
        "GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de "
        "garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la <a href=\"https://www.gnu.org/licenses/\">Licencia "
        "Pública General</a> de GNU para más detalles.</font></p>"
        );

}
