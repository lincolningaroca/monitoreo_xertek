/********************************************************************************
** Form generated from reading UI file 'nuevaestmonitoreodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NUEVAESTMONITOREODIALOG_H
#define UI_NUEVAESTMONITOREODIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NuevaEstMonitoreoDialog
{
public:
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label;
    QComboBox *cboGrupo;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QComboBox *cboUnidad;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *txtPath1;
    QToolButton *btnFoto1;
    QHBoxLayout *horizontalLayout_8;
    QLineEdit *txtPath2;
    QToolButton *btnFoto2;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *txtPath3;
    QToolButton *btnFoto3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLineEdit *txtCodigo;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QDateEdit *deFecha;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QTimeEdit *teHora;
    QLabel *label_14;
    QLabel *label_7;
    QListWidget *lwProcedencia;
    QComboBox *cboReferencia;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout;
    QFormLayout *formLayout_3;
    QLabel *label_9;
    QDoubleSpinBox *dsbPh;
    QFormLayout *formLayout_4;
    QLabel *label_11;
    QDoubleSpinBox *dsbTemp;
    QFormLayout *formLayout_5;
    QLabel *label_10;
    QDoubleSpinBox *dsbOd;
    QFormLayout *formLayout_6;
    QLabel *label_12;
    QDoubleSpinBox *dsbCond;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_3;
    QLabel *label_6;
    QDoubleSpinBox *dsbEste;
    QLabel *label_8;
    QDoubleSpinBox *dsbNorte;
    QLabel *label_13;
    QDoubleSpinBox *dsbCota;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *btnProcedencia;
    QToolButton *btnReferencia;
    QFrame *line;
    QToolButton *btnBuscarProc;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnNuevoGrupo;
    QPushButton *btnNuevaUnnidad;
    QPushButton *btnEditar;
    QFrame *line_2;
    QPushButton *btnGuardar;
    QPushButton *btnCancelar;

    void setupUi(QDialog *NuevaEstMonitoreoDialog)
    {
        if (NuevaEstMonitoreoDialog->objectName().isEmpty())
            NuevaEstMonitoreoDialog->setObjectName(QString::fromUtf8("NuevaEstMonitoreoDialog"));
        NuevaEstMonitoreoDialog->resize(742, 428);
        gridLayout_4 = new QGridLayout(NuevaEstMonitoreoDialog);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        groupBox = new QGroupBox(NuevaEstMonitoreoDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_6->addWidget(label);

        cboGrupo = new QComboBox(groupBox);
        cboGrupo->setObjectName(QString::fromUtf8("cboGrupo"));
        cboGrupo->setMinimumSize(QSize(180, 0));
        cboGrupo->setMaximumSize(QSize(180, 16777215));

        horizontalLayout_6->addWidget(cboGrupo);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_4->addWidget(label_2);

        cboUnidad = new QComboBox(groupBox);
        cboUnidad->setObjectName(QString::fromUtf8("cboUnidad"));
        cboUnidad->setMinimumSize(QSize(180, 0));
        cboUnidad->setMaximumSize(QSize(180, 16777215));

        horizontalLayout_4->addWidget(cboUnidad);


        verticalLayout_2->addLayout(horizontalLayout_4);


        gridLayout_4->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(NuevaEstMonitoreoDialog);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        txtPath1 = new QLineEdit(groupBox_3);
        txtPath1->setObjectName(QString::fromUtf8("txtPath1"));
        txtPath1->setMinimumSize(QSize(300, 0));
        txtPath1->setMaximumSize(QSize(300, 16777215));
        txtPath1->setReadOnly(true);

        horizontalLayout_7->addWidget(txtPath1);

        btnFoto1 = new QToolButton(groupBox_3);
        btnFoto1->setObjectName(QString::fromUtf8("btnFoto1"));

        horizontalLayout_7->addWidget(btnFoto1);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        txtPath2 = new QLineEdit(groupBox_3);
        txtPath2->setObjectName(QString::fromUtf8("txtPath2"));
        txtPath2->setMinimumSize(QSize(300, 0));
        txtPath2->setMaximumSize(QSize(300, 16777215));
        txtPath2->setReadOnly(true);

        horizontalLayout_8->addWidget(txtPath2);

        btnFoto2 = new QToolButton(groupBox_3);
        btnFoto2->setObjectName(QString::fromUtf8("btnFoto2"));

        horizontalLayout_8->addWidget(btnFoto2);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        txtPath3 = new QLineEdit(groupBox_3);
        txtPath3->setObjectName(QString::fromUtf8("txtPath3"));
        txtPath3->setMinimumSize(QSize(300, 0));
        txtPath3->setMaximumSize(QSize(300, 16777215));
        txtPath3->setReadOnly(true);

        horizontalLayout_9->addWidget(txtPath3);

        btnFoto3 = new QToolButton(groupBox_3);
        btnFoto3->setObjectName(QString::fromUtf8("btnFoto3"));

        horizontalLayout_9->addWidget(btnFoto3);


        verticalLayout->addLayout(horizontalLayout_9);


        gridLayout_4->addWidget(groupBox_3, 0, 1, 2, 1);

        groupBox_2 = new QGroupBox(NuevaEstMonitoreoDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        txtCodigo = new QLineEdit(groupBox_2);
        txtCodigo->setObjectName(QString::fromUtf8("txtCodigo"));
        txtCodigo->setReadOnly(false);

        horizontalLayout->addWidget(txtCodigo);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_3->addWidget(label_4);

        deFecha = new QDateEdit(groupBox_2);
        deFecha->setObjectName(QString::fromUtf8("deFecha"));
        deFecha->setReadOnly(false);
        deFecha->setCalendarPopup(true);

        horizontalLayout_3->addWidget(deFecha);


        gridLayout_2->addLayout(horizontalLayout_3, 1, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_5->addWidget(label_5);

        teHora = new QTimeEdit(groupBox_2);
        teHora->setObjectName(QString::fromUtf8("teHora"));
        teHora->setReadOnly(false);
        teHora->setCalendarPopup(false);

        horizontalLayout_5->addWidget(teHora);


        gridLayout_2->addLayout(horizontalLayout_5, 2, 0, 1, 1);

        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 7, 0, 1, 1);

        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_2->addWidget(label_7, 3, 0, 1, 1);

        lwProcedencia = new QListWidget(groupBox_2);
        lwProcedencia->setObjectName(QString::fromUtf8("lwProcedencia"));

        gridLayout_2->addWidget(lwProcedencia, 5, 0, 1, 1);

        cboReferencia = new QComboBox(groupBox_2);
        cboReferencia->setObjectName(QString::fromUtf8("cboReferencia"));

        gridLayout_2->addWidget(cboReferencia, 8, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_2, 1, 0, 3, 1);

        groupBox_4 = new QGroupBox(NuevaEstMonitoreoDialog);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout = new QGridLayout(groupBox_4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_9);

        dsbPh = new QDoubleSpinBox(groupBox_4);
        dsbPh->setObjectName(QString::fromUtf8("dsbPh"));
        dsbPh->setMinimumSize(QSize(133, 0));
        dsbPh->setMaximumSize(QSize(133, 16777215));
        dsbPh->setReadOnly(false);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, dsbPh);


        gridLayout->addLayout(formLayout_3, 0, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_11);

        dsbTemp = new QDoubleSpinBox(groupBox_4);
        dsbTemp->setObjectName(QString::fromUtf8("dsbTemp"));
        dsbTemp->setMinimumSize(QSize(133, 0));
        dsbTemp->setMaximumSize(QSize(133, 16777215));
        dsbTemp->setReadOnly(false);
        dsbTemp->setMaximum(999.990000000000009);

        formLayout_4->setWidget(0, QFormLayout::FieldRole, dsbTemp);


        gridLayout->addLayout(formLayout_4, 0, 1, 1, 1);

        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_10);

        dsbOd = new QDoubleSpinBox(groupBox_4);
        dsbOd->setObjectName(QString::fromUtf8("dsbOd"));
        dsbOd->setMinimumSize(QSize(133, 0));
        dsbOd->setMaximumSize(QSize(133, 16777215));
        dsbOd->setReadOnly(false);

        formLayout_5->setWidget(0, QFormLayout::FieldRole, dsbOd);


        gridLayout->addLayout(formLayout_5, 1, 0, 1, 1);

        formLayout_6 = new QFormLayout();
        formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_12);

        dsbCond = new QDoubleSpinBox(groupBox_4);
        dsbCond->setObjectName(QString::fromUtf8("dsbCond"));
        dsbCond->setMinimumSize(QSize(133, 0));
        dsbCond->setMaximumSize(QSize(133, 16777215));
        dsbCond->setReadOnly(false);
        dsbCond->setMaximum(999999999.990000009536743);

        formLayout_6->setWidget(0, QFormLayout::FieldRole, dsbCond);


        gridLayout->addLayout(formLayout_6, 1, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_4, 2, 1, 1, 1);

        groupBox_5 = new QGroupBox(NuevaEstMonitoreoDialog);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        gridLayout_3 = new QGridLayout(groupBox_5);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_6 = new QLabel(groupBox_5);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_3->addWidget(label_6, 0, 0, 1, 1);

        dsbEste = new QDoubleSpinBox(groupBox_5);
        dsbEste->setObjectName(QString::fromUtf8("dsbEste"));
        dsbEste->setMinimumSize(QSize(120, 0));
        dsbEste->setMaximumSize(QSize(120, 16777215));
        dsbEste->setMaximum(9999999999.000000000000000);

        gridLayout_3->addWidget(dsbEste, 0, 1, 1, 1);

        label_8 = new QLabel(groupBox_5);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_3->addWidget(label_8, 0, 2, 1, 1);

        dsbNorte = new QDoubleSpinBox(groupBox_5);
        dsbNorte->setObjectName(QString::fromUtf8("dsbNorte"));
        dsbNorte->setMinimumSize(QSize(120, 0));
        dsbNorte->setMaximumSize(QSize(120, 16777215));
        dsbNorte->setMaximum(9999999999.000000000000000);

        gridLayout_3->addWidget(dsbNorte, 0, 3, 1, 1);

        label_13 = new QLabel(groupBox_5);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_3->addWidget(label_13, 1, 0, 1, 1);

        dsbCota = new QDoubleSpinBox(groupBox_5);
        dsbCota->setObjectName(QString::fromUtf8("dsbCota"));
        dsbCota->setMinimumSize(QSize(150, 0));
        dsbCota->setMaximumSize(QSize(150, 16777215));
        dsbCota->setMaximum(9999999999.000000000000000);

        gridLayout_3->addWidget(dsbCota, 1, 1, 1, 1);

        pushButton = new QPushButton(groupBox_5);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/ui_-09-128.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon);

        gridLayout_3->addWidget(pushButton, 1, 3, 1, 1);


        gridLayout_4->addWidget(groupBox_5, 3, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btnProcedencia = new QToolButton(NuevaEstMonitoreoDialog);
        btnProcedencia->setObjectName(QString::fromUtf8("btnProcedencia"));

        horizontalLayout_2->addWidget(btnProcedencia);

        btnReferencia = new QToolButton(NuevaEstMonitoreoDialog);
        btnReferencia->setObjectName(QString::fromUtf8("btnReferencia"));

        horizontalLayout_2->addWidget(btnReferencia);

        line = new QFrame(NuevaEstMonitoreoDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line);

        btnBuscarProc = new QToolButton(NuevaEstMonitoreoDialog);
        btnBuscarProc->setObjectName(QString::fromUtf8("btnBuscarProc"));

        horizontalLayout_2->addWidget(btnBuscarProc);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnNuevoGrupo = new QPushButton(NuevaEstMonitoreoDialog);
        btnNuevoGrupo->setObjectName(QString::fromUtf8("btnNuevoGrupo"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/img/15552 - choose view.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnNuevoGrupo->setIcon(icon1);
        btnNuevoGrupo->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnNuevoGrupo);

        btnNuevaUnnidad = new QPushButton(NuevaEstMonitoreoDialog);
        btnNuevaUnnidad->setObjectName(QString::fromUtf8("btnNuevaUnnidad"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/img/15553 - detailed view.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnNuevaUnnidad->setIcon(icon2);
        btnNuevaUnnidad->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnNuevaUnnidad);

        btnEditar = new QPushButton(NuevaEstMonitoreoDialog);
        btnEditar->setObjectName(QString::fromUtf8("btnEditar"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/img/15559 - sidetree view.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnEditar->setIcon(icon3);
        btnEditar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnEditar);

        line_2 = new QFrame(NuevaEstMonitoreoDialog);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line_2);

        btnGuardar = new QPushButton(NuevaEstMonitoreoDialog);
        btnGuardar->setObjectName(QString::fromUtf8("btnGuardar"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/img/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnGuardar->setIcon(icon4);
        btnGuardar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnGuardar);

        btnCancelar = new QPushButton(NuevaEstMonitoreoDialog);
        btnCancelar->setObjectName(QString::fromUtf8("btnCancelar"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/img/cancel.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnCancelar->setIcon(icon5);
        btnCancelar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnCancelar);


        gridLayout_4->addLayout(horizontalLayout_2, 4, 0, 1, 2);


        retranslateUi(NuevaEstMonitoreoDialog);

        QMetaObject::connectSlotsByName(NuevaEstMonitoreoDialog);
    } // setupUi

    void retranslateUi(QDialog *NuevaEstMonitoreoDialog)
    {
        NuevaEstMonitoreoDialog->setWindowTitle(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Dialog", nullptr));
        groupBox->setTitle(QString());
        label->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Compa\303\261\303\255a minera:", nullptr));
        label_2->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Unidad minera:", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Fotos", nullptr));
        txtPath1->setPlaceholderText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Primera foto", nullptr));
#if QT_CONFIG(tooltip)
        btnFoto1->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cargar primera imagen (F1)", nullptr));
#endif // QT_CONFIG(tooltip)
        btnFoto1->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        btnFoto1->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "F1", nullptr));
#endif // QT_CONFIG(shortcut)
        txtPath2->setPlaceholderText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Segunda foto", nullptr));
#if QT_CONFIG(tooltip)
        btnFoto2->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cargar segunda imagen (F2)", nullptr));
#endif // QT_CONFIG(tooltip)
        btnFoto2->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        btnFoto2->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "F2", nullptr));
#endif // QT_CONFIG(shortcut)
        txtPath3->setPlaceholderText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Tercera foto", nullptr));
#if QT_CONFIG(tooltip)
        btnFoto3->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cargar tercera imagen (F3)", nullptr));
#endif // QT_CONFIG(tooltip)
        btnFoto3->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(shortcut)
        btnFoto3->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "F3", nullptr));
#endif // QT_CONFIG(shortcut)
        groupBox_2->setTitle(QString());
        label_3->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "C\303\263digo de estaci\303\263n:", nullptr));
        txtCodigo->setPlaceholderText(QString());
        label_4->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Fecha:", nullptr));
        label_5->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Hora:", nullptr));
        label_14->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Referencia:", nullptr));
        label_7->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Procedencia:", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Par\303\241metros de campo", nullptr));
        label_9->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "PH:", nullptr));
        label_11->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Temp:", nullptr));
        label_10->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "OD:", nullptr));
        label_12->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cond:", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Coordenadas", nullptr));
        label_6->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Este:", nullptr));
        label_8->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Norte:", nullptr));
        label_13->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cota:", nullptr));
        dsbCota->setSuffix(QCoreApplication::translate("NuevaEstMonitoreoDialog", "  (m.s.n.m)", nullptr));
#if QT_CONFIG(tooltip)
        pushButton->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "(Ctrl+D) Para abrir ventana", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Descripci\303\263n del punto", nullptr));
#if QT_CONFIG(shortcut)
        pushButton->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Ctrl+D", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnProcedencia->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Agregar nueva procedencia", nullptr));
#endif // QT_CONFIG(tooltip)
        btnProcedencia->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(tooltip)
        btnReferencia->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Nueva referencia", nullptr));
#endif // QT_CONFIG(tooltip)
        btnReferencia->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(tooltip)
        btnBuscarProc->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Buscar Procedencia", nullptr));
#endif // QT_CONFIG(tooltip)
        btnBuscarProc->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "...", nullptr));
#if QT_CONFIG(tooltip)
        btnNuevoGrupo->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Nuevo grupo minero", nullptr));
#endif // QT_CONFIG(tooltip)
        btnNuevoGrupo->setText(QString());
#if QT_CONFIG(tooltip)
        btnNuevaUnnidad->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Nueva unidad minera", nullptr));
#endif // QT_CONFIG(tooltip)
        btnNuevaUnnidad->setText(QString());
#if QT_CONFIG(shortcut)
        btnNuevaUnnidad->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Enter", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnEditar->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Editar datos - Punto de monitoreo", nullptr));
#endif // QT_CONFIG(tooltip)
        btnEditar->setText(QString());
#if QT_CONFIG(tooltip)
        btnGuardar->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "(Ctrl+S) Para guardar", nullptr));
#endif // QT_CONFIG(tooltip)
        btnGuardar->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Guardar", nullptr));
#if QT_CONFIG(shortcut)
        btnGuardar->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnCancelar->setToolTip(QCoreApplication::translate("NuevaEstMonitoreoDialog", "(Esc) Para Cancelar", nullptr));
#endif // QT_CONFIG(tooltip)
        btnCancelar->setText(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Cancelar", nullptr));
#if QT_CONFIG(shortcut)
        btnCancelar->setShortcut(QCoreApplication::translate("NuevaEstMonitoreoDialog", "Esc", nullptr));
#endif // QT_CONFIG(shortcut)
    } // retranslateUi

};

namespace Ui {
    class NuevaEstMonitoreoDialog: public Ui_NuevaEstMonitoreoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NUEVAESTMONITOREODIALOG_H
