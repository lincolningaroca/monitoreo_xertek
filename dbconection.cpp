#include "dbconection.h"
//#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>

//DbConection::DbConection() {}

bool DbConection::connect(const QString &dbName) {
  m_dbName = dbName;
  QSqlDatabase db;
  if (!db.isDriverAvailable("QPSQL")) {
    m_errorMessage = db.lastError().driverText();
    //      qDebug()<<_errorMessage;
    return false;
  }
  db = QSqlDatabase::addDatabase("QPSQL", dbName);
  db.setPort(5432);
  db.setHostName("127.0.0.1");
  db.setDatabaseName("monitoreo_db");
  db.setUserName("postgres");
  db.setPassword("2311046");

  if (!db.open()) {
    m_errorMessage = db.lastError().databaseText();
    //      qDebug()<<_errorMessage;
    return false;
  }

  return true;
}

QSqlDatabase DbConection::getDataBase() const noexcept {
  return QSqlDatabase::database(m_dbName);
}

void DbConection::closeCn() { getDataBase().close(); }
