QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
#QT_DEBUG_PLUGINS +=1

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    editarefproc.cpp \
    frmacercade.cpp \
    frmprincipal.cpp \
    generareferenciadialog.cpp \
 main.cpp\
     # mainwindow.cpp\
      dbconection.cpp\
    nuevaestmonitdialog.cpp \
      nuevovarios.cpp \
    referenciadialog.cpp \
      swcustomtxt.cpp\
      bussineslayer.cpp\
      nuevodialog.cpp\
      qeasysettings.cpp\
      editdatadialog.cpp\
      desc_pdialog.cpp\
      fotodialog.cpp
HEADERS += \
    editarefproc.h \
    frmacercade.h \
    frmprincipal.h \
 # mainwindow.h\
      dbconection.h\
    generareferenciadialog.h \
    nuevaestmonitdialog.h \
      nuevovarios.h \
    referenciadialog.h \
      swcustomtxt.h\
      bussineslayer.h\
      nuevodialog.h\
      qeasysettings.hpp\
      editdatadialog.h\
      desc_pdialog.h\
      fotodialog.h \
    typealiases.hpp
FORMS += \
    editarefproc.ui \
    frmacercade.ui \
    frmprincipal.ui \
 #mainwindow.ui\
    generareferenciadialog.ui \
    nuevaestmonitdialog.ui \
      nuevodialog.ui\
      editdatadialog.ui\
      Desc_pdialog.ui\
      fotodialog.ui \
      nuevovarios.ui \
    referenciadialog.ui
RESOURCES +=\
  Docs.qrc \
  rsc.qrc

RC_ICONS+=AppIcon/appIcon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
