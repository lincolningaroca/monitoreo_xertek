#include "editarefproc.h"
#include "ui_editarefproc.h"
//#include <QDebug>
#include <QMessageBox>

EditaRefProc::EditaRefProc(unsigned int op, QVariantList value, QWidget *parent)
  : QDialog(parent), ui(new Ui::EditaRefProc), m_op(op), m_value(value) {

  ui->setupUi(this);

  if (m_op == 1) {
      setWindowTitle(qApp->applicationName().append(" - Editar Nombre de la procedencia"));
      ui->groupBox->setTitle("Cambiar nombre de la procedencia");
      ui->ptValue->setPlainText(m_value.value(0).toString());
      //      qDebug()<<_value.value(1);

    } else {
      setWindowTitle(qApp->applicationName().append(" - Editar Nombre de la referencia"));
      ui->groupBox->setTitle("Cambiar nombre de la referencia");
      ui->ptValue->setPlainText(m_value.value(0).toString());
      //      qDebug()<<_value.value(1);
    }
}

EditaRefProc::~EditaRefProc() { delete ui;}

void EditaRefProc::on_btnCancelar_clicked() { reject(); }

void EditaRefProc::on_btnGuardar_clicked() {
  if (m_op == 1) {
      if (ui->ptValue->toPlainText().simplified().isEmpty()) {
          QMessageBox::warning(this, qApp->applicationName(), "Ingrese una procedencia.");
          ui->ptValue->setFocus(Qt::OtherFocusReason);
          return;
        }
      QVariantList param{};
      param.append(ui->ptValue->toPlainText().toUpper());
      param.append(m_value.value(1).toInt());
      param.append(m_value.value(2).toInt());
      if (!m_bLayer.tablaMonitoreo(param, BussinesLayer::TypeStm::UPDATE)) {
          QMessageBox::warning(this, qApp->applicationName(),"Error al insertar los datos.\n" +
                               m_bLayer.errorMessage());
          return;
        }
      QMessageBox::information(this, qApp->applicationName(),"Los datos fueron cambiados.");
      accept();
    } else {
      if (ui->ptValue->toPlainText().simplified().isEmpty()) {
          QMessageBox::warning(this, qApp->applicationName(),"Ingrese una referencia.");
          ui->ptValue->setFocus(Qt::OtherFocusReason);
          return;
        }
      QVariantList param{};
      param.append(ui->ptValue->toPlainText().toUpper());
      param.append(m_value.value(1).toInt());
      param.append(m_value.value(2).toString());
      param.append(m_value.value(3).toString());
      param.append(m_value.value(4).toString());
      param.append(m_value.value(5).toInt());
      if (!m_bLayer.tablaReferencia(param, BussinesLayer::TypeStm::UPDATE)) {
          QMessageBox::warning(this, qApp->applicationName(),"Error al insertar los datos.\n" +
                               m_bLayer.errorMessage());
          return;
        }
      QMessageBox::information(this, qApp->applicationName(),"Los datos fueron cambiados.");
      accept();
    }
}
