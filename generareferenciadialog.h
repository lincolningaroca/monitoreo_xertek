#pragma once

#include "bussineslayer.h"
#include <QDialog>

namespace Ui {
  class GeneraReferenciaDialog;
}

class GeneraReferenciaDialog : public QDialog {
  Q_OBJECT

public:
  explicit GeneraReferenciaDialog(unsigned int id_cliente,unsigned int id_monit,QWidget *parent = nullptr);
  ~GeneraReferenciaDialog();

  void loadProcedencia();
  void loadReferencias();

private slots:
  void on_btnCancelar_clicked();
  void on_btnGenerar_clicked();

private:
  Ui::GeneraReferenciaDialog *ui;
  BussinesLayer m_bLayer{};
  QHash<unsigned int, QString> m_dataProcedencia{};
  QHash<unsigned int, QString> m_dataReferencia{};
  const unsigned int m_idCliente{0};
  const unsigned int m_idMonit{0};

  void checkRefExists();
};


