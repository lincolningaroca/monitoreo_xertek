#include "nuevodialog.h"
#include "ui_nuevodialog.h"
#include <QMessageBox>
#include <QListWidgetItem>


NuevoDialog::NuevoDialog(DialogMode mode, QVariantList data, QWidget *parent) :
  QDialog(parent), ui(new Ui::NuevoDialog), m_mode(mode), m_data(data)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  setUpForm();
  if(!m_data.isEmpty())
    loadDataListCliente();
}

NuevoDialog::~NuevoDialog()
{
  delete ui;
}

void NuevoDialog::loadDataList()
{
  m_dataList=m_bussines.gpoMineroList(BussinesLayer::Table::GPO_MINERO);
  ui->cboLista->addItems(m_dataList.values());
}

void NuevoDialog::on_btnGuardar_clicked()
{

  if(ui->txtNombre->text().simplified().isEmpty()){

      QMessageBox::warning(this,qApp->applicationName(),
                           "El campo nombre no puede contener espacios en blanco al principio o al final.\n"
                           "Así mismo, Verifique que el campo no este vacío.");
      ui->txtNombre->selectAll();
      ui->txtNombre->setFocus();
      return;
    }
  if(m_mode==DialogMode::GRUPO){
      QVariantList param{};
      param<<ui->txtNombre->text().toUpper()<<ui->teDescripcion->toPlainText().toUpper();
      if(!m_bussines.gpoMineroAction(param,BussinesLayer::TypeStm::INSERT)){
          if(m_bussines.errorCode()=="23505"){
              QMessageBox::warning(this,qApp->applicationName(),
                                   "Este nombre ya esta registrado en la base de datos.\n"
                                   "Por favor intente con uno nuevo.");
              ui->txtNombre->selectAll();
              ui->txtNombre->setFocus(Qt::OtherFocusReason);
              return;
            }
        }
      //      QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
      //      accept();
    }else if(m_mode==DialogMode::UNIDAD){
      QVariantList param{};
      param.append(ui->txtNombre->text().toUpper());
      param.append(ui->teDescripcion->toPlainText().toUpper());
      param.append(m_dataList.key(ui->cboLista->currentText()));

      if(!m_bussines.clienteMineroAction(param,BussinesLayer::TypeStm::INSERT)){
          if(m_bussines.errorCode()=="23505"){
              QMessageBox::warning(this,qApp->applicationName(),
                                   "Este nombre ya esta registrado en la base de datos.\n"
                                   "Por favor intente con uno nuevo.");
              ui->txtNombre->selectAll();
              ui->txtNombre->setFocus(Qt::OtherFocusReason);
              return;
            }
        }
      //      QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
      //      accept();
    }else if(m_mode==DialogMode::UPDATE_GPO){
      QVariantList paramg{};
      paramg.append(ui->txtNombre->text().toUpper());
      paramg.append(ui->teDescripcion->toPlainText().toUpper());
      paramg.append(m_data.value(0));
      if(!m_bussines.gpoMineroAction(paramg,BussinesLayer::TypeStm::UPDATE)){
          if(m_bussines.errorCode()=="23505"){
              QMessageBox::warning(this,qApp->applicationName(),
                                   "Este nombre ya esta registrado en la base de datos.\n"
                                   "Por favor intente con uno nuevo.");
              ui->txtNombre->selectAll();
              ui->txtNombre->setFocus(Qt::OtherFocusReason);
              return;
            }
        }
      //      QMessageBox::information(this,qApp->applicationName(),"Datos actualizados.");
      //      accept();
    }else{
      QVariantList param{};
      param.append(ui->txtNombre->text().toUpper());
      param.append(ui->teDescripcion->toPlainText().toUpper());
      param.append(m_dataList.key(ui->cboLista->currentText()));
      param.append(m_dataListCliente.key(ui->lwUnidades->currentItem()->data(Qt::DisplayRole).toString()));
      if(!m_bussines.clienteMineroAction(param,BussinesLayer::TypeStm::UPDATE)){
          if(m_bussines.errorCode()=="23505"){
              QMessageBox::warning(this,qApp->applicationName(),
                                   "Este nombre ya esta registrado en la base de datos.\n"
                                   "Por favor intente con uno nuevo.");
              ui->txtNombre->selectAll();
              ui->txtNombre->setFocus(Qt::OtherFocusReason);
              return;
            }
        }

    }

  QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
  accept();


}

void NuevoDialog::on_btnCancelar_clicked()
{
  reject();
}
void NuevoDialog::loadDataListCliente()
{
  m_dataListCliente=m_bussines.gpoMineroList(BussinesLayer::Table::CLIENTE,
                                         m_dataList.key(ui->cboLista->currentText()));
  //m_dataListCliente=m_bussines.gpoMineroList(m_bussinesLayer::CLIENTE,38);
  QStringList values=m_dataListCliente.values();
  for(const auto& i : values){
      QListWidgetItem *item=new QListWidgetItem(QIcon(":/img/ui-11-128.png"),
                                                i,ui->lwUnidades);
      ui->lwUnidades->addItem(item);
    }

  if(m_mode==DialogMode::UPDATE_UNIDAD){
      ui->lwUnidades->setCurrentRow(0);
      on_lwUnidades_itemClicked(ui->lwUnidades->currentItem());
      //    QObject::connect(ui->lwUnidades->selectionModel(),&QItemSelectionModel::currentChanged,this,
      //                     [&](){
      //                       on_lwUnidades_itemClicked(ui->lwUnidades->currentItem());
      //                     });
    }

}

void NuevoDialog::on_cboLista_activated(int index)
{
  Q_UNUSED(index)
  ui->lwUnidades->clear();
  loadDataListCliente();
  //  qDebug()<<m_dataList.key(ui->cboLista->currentText());
  //  qDebug()<<m_dataListCliente.value(1);

}

void NuevoDialog::setUpForm()
{
  switch (m_mode) {
    case DialogMode::GRUPO:
      ui->lblNombre->setText("Nombre - Grupo o compañía minera: ");
      ui->lblGrupo->setVisible(false);
      ui->cboLista->setVisible(false);
      ui->lwUnidades->setVisible(false);
      break;
    case DialogMode::UPDATE_GPO:
      ui->lblNombre->setText("Nombre - Grupo o compañía minera: ");
      ui->lblGrupo->setVisible(false);
      ui->cboLista->setVisible(false);
      ui->lwUnidades->setVisible(false);
      ui->txtNombre->setText(m_data.value(1).toString());
      ui->teDescripcion->setPlainText(m_data.value(2).toString());
      break;

    case DialogMode::UNIDAD:
      ui->lblNombre->setText("Nombre - unidad minera: ");
      ui->lblGrupo->setVisible(true);
      ui->cboLista->setVisible(true);
      ui->lwUnidades->setVisible(false);
      loadDataList();
      break;
    case DialogMode::UPDATE_UNIDAD:
      ui->lblNombre->setText("Nombre - unidad minera: ");
      ui->lblGrupo->setVisible(true);
      ui->cboLista->setVisible(true);
      loadDataList();
      break;

    default:
      break;

    }

}

void NuevoDialog::on_lwUnidades_itemClicked(QListWidgetItem *item)
{
  QVariantList dataCte=m_bussines.selectData(
        m_dataListCliente.key(item->data(Qt::DisplayRole).toString()),BussinesLayer::Table::CLIENTE);
  ui->txtNombre->setText(dataCte.value(1).toString());
  ui->teDescripcion->setPlainText(dataCte.value(2).toString());
}

