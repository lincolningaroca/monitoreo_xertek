#pragma once

#include <QDialog>
#include "bussineslayer.h"

namespace Ui {
  class ReferenciaDialog;
}

class ReferenciaDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ReferenciaDialog(QWidget *parent = nullptr);
  ~ReferenciaDialog();

private slots:
  void on_btnAceptar_clicked();
  void on_cboCliente_activated(int index);
  void on_btnCancelar_clicked();
  void on_cboMatriz_activated(const QString &arg1);
  void on_btnNuevaProcedencia_clicked();


private:
  Ui::ReferenciaDialog *ui;
  QHash<unsigned int,QString> m_dataClient{};
  QHash<unsigned int,QString> m_dataProcedencia{};
  BussinesLayer m_bLayer{};
  QHash<QString,QString> m_matrizList{};

  void loadDataClient() noexcept;
  void loadDataProcedencia() noexcept;
  void fillCboMatriz() noexcept;
  void fillCboNComparacion() noexcept;
};


