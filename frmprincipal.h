#pragma once

#include <QMainWindow>
#include "bussineslayer.h"

namespace Ui {
  class FrmPrincipal;
}
class QComboBox;
class QListWidgetItem;

class FrmPrincipal : public QMainWindow {
  Q_OBJECT

public:
  explicit FrmPrincipal(QWidget *parent = nullptr);
  ~FrmPrincipal();

  void loadGpoMinerolist() noexcept;
  void loadDataListCliente() noexcept;
  void loadDataEstMonitoreo() noexcept;
  void saveImageContextMenu() noexcept;
  void loadCboTemas() noexcept;
  void readSettings() noexcept;
  void writeSettings() noexcept;
  //  static MainWindow *getInstance();
  void haveData() noexcept;

private:
  Ui::FrmPrincipal *ui;
  BussinesLayer m_bLayer{};
  QHash<unsigned int, QString> m_dataList{};
  QVariantList m_dataList_monitoreo{};
  QHash<unsigned int, QString> m_dataListCliente{};
  QComboBox *m_cboTemas{nullptr};
  //  QHash<unsigned int, QString> meses{};
  QHash<unsigned int, QString> m_dataList_2{};
  QHash<unsigned int, QString> m_dataTableProc{};
  QHash<unsigned int, QString> m_dataTableRef{};
  //  int anio{0};
  QImage m_foto_1{};
  QImage m_foto_2{};
  QImage m_foto_3{};

  void datosMonitoreo() noexcept;
  void cleanControls() noexcept;
  void defaultImage() noexcept;
  QImage openPicture(const QString &f) noexcept;
  void showFotoToControl(const QListWidgetItem *item) noexcept;
  void tableProcedencia() noexcept;
  void tableReferencia() noexcept;
  unsigned int indexRnd(int) const noexcept;
  void loadPictures() noexcept;
  QString setPathToSave(unsigned) const noexcept;
  // QPaintDevice interface
public:
  virtual QPaintEngine *paintEngine() const override;

  // QWidget interface
protected:
  virtual void closeEvent(QCloseEvent *event) override;
private slots:
  void on_toolButton_clicked();
  void on_toolButton_2_clicked();
  void on_cboGrupo_activated(int index);
  void on_actionNuevo_punto_de_monitoreo_triggered();
  void on_cboUnidad_activated(int index);
  void on_actionEditar_datos_grupo_o_consorcio_minero_triggered();
  void on_actionEditar_datos_unidad_minera_triggered();
  void on_actioneditar_datos_monitoreo_triggered();
  void on_lwEstaciones_itemSelectionChanged();
  void on_lwFotos_itemSelectionChanged();
  void on_lwProcedencia_itemSelectionChanged();
  void on_cboReferencia_activated(int index);
  void on_actionAcerca_de_triggered();
  // QObject interface
  void on_actionSalir_triggered();

public:
  virtual bool eventFilter(QObject *watched, QEvent *event) override;
};


