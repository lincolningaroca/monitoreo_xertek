#include "frmprincipal.h"
#include "desc_pdialog.h"
#include "editdatadialog.h"
#include "frmacercade.h"
#include "nuevaestmonitdialog.h"
#include "nuevodialog.h"
#include "qeasysettings.hpp"
#include "ui_frmprincipal.h"
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QPaintEngine>
#include <QRandomGenerator>
#include <QSettings>
#include <QStandardPaths>

FrmPrincipal::FrmPrincipal(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::FrmPrincipal) {
  ui->setupUi(this);

  ui->deFecha->setDate(QDate::currentDate());
  ui->teHora->setTime(QTime::currentTime());
  loadCboTemas();

  saveImageContextMenu();
  loadGpoMinerolist();
  loadDataListCliente();
  tableProcedencia();
  tableReferencia();
  loadDataEstMonitoreo();
  defaultImage();
  haveData();

  QObject::connect(ui->txtDesc_punto, &SWCustomTxt::clicked, this, [&]() {
      Desc_pDialog desc_dialog(1, ui->txtDesc_punto->text(), this);
      desc_dialog.exec();
    });
  //  qDebug()<<"tura relativa: "<<m_bLayer.relativePath();
  readSettings();

  this->centralWidget()->installEventFilter(this);
}

FrmPrincipal::~FrmPrincipal() { delete ui; }

void FrmPrincipal::loadGpoMinerolist() noexcept{
  m_dataList = m_bLayer.gpoMineroList(BussinesLayer::Table::GPO_MINERO);

  if (m_dataList.isEmpty()) {
      QMessageBox::critical(this, qApp->applicationName(), m_bLayer.errorMessage());
      return;
    }
  ui->cboGrupo->addItems(m_dataList.values());

}

void FrmPrincipal::loadDataListCliente() noexcept{

  ui->cboUnidad->clear();
  m_dataListCliente = m_bLayer.gpoMineroList(BussinesLayer::Table::CLIENTE,
                                             m_dataList.key(ui->cboGrupo->currentText()));
  if (!m_dataListCliente.isEmpty()) {
      ui->cboUnidad->addItems(m_dataListCliente.values());
    }
}

void FrmPrincipal::loadDataEstMonitoreo() noexcept{

  ui->lwEstaciones->clear();

  m_dataList_2 = m_bLayer.selectCodEstacion(m_dataTableRef.key(ui->cboReferencia->currentText()));
  QStringList l = m_dataList_2.values();
  //  QListWidgetItem *item{nullptr};
  for (const auto& i: l) {
      QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui-11-128.png"), i,ui->lwEstaciones);
      ui->lwEstaciones->addItem(item);
    }

  ui->lwEstaciones->setCurrentItem(ui->lwEstaciones->item(indexRnd(ui->lwEstaciones->count() - 1)),
                                   QItemSelectionModel::Select);
}

void FrmPrincipal::saveImageContextMenu() noexcept{
  ui->lblfoto->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui->lblfoto->addAction(ui->actionGuardar_foto);
  ui->lblfoto->addAction(ui->actionGuardar_foto_como);

  QObject::connect(ui->actionGuardar_foto, &QAction::triggered, this, [&]() {
      QStringList paths = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
      QString& pathToSave = paths.first();
      pathToSave.append("/");
      pathToSave.append(ui->cboUnidad->currentText());
      pathToSave.append("_");
      pathToSave.append(ui->txtEstacion->text());
      pathToSave.append("_");
      pathToSave.append(ui->deFecha->date().toString());
      pathToSave.append("_");

      QFile file{};
      QFileInfo info{};

      QString currentPicture = ui->lwFotos->currentItem()->data(Qt::DisplayRole).toString();
      if (currentPicture.compare("Foto 1") == 0) {
          file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(3).toString()));
          info.setFile(file);
          pathToSave.append(info.fileName());

        } else if (currentPicture.compare("Foto 2") == 0) {
          file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(4).toString()));
          info.setFile(file);
          pathToSave.append(info.fileName());

        } else {
          file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(5).toString()));
          info.setFile(file);
          pathToSave.append(info.fileName());

        }


      if (!file.copy(pathToSave)) {
          QMessageBox::warning(this, qApp->applicationName(),"Ya existe un archivo con este nombre.\n" +
                               file.errorString());
          return;
        }
      QMessageBox::information(this, qApp->applicationName(),"Archivo guardado en:\n" + pathToSave);
    });

  QObject::connect(ui->actionGuardar_foto_como, &QAction::triggered, this, [&]() {

      QString fileName = QFileDialog::getSaveFileName(this, "Guardar foto", QDir::current().path(),
                                                      "Imagenes (*.jpg)");
      if (fileName.isEmpty())
        return;
      QFile file{};
      QString currentPicture = ui->lwFotos->currentItem()->data(Qt::DisplayRole).toString();
      //      if (!currentPicture.isEmpty()) {
      if (currentPicture.compare("Foto 1") == 0)
        file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(3).toString()));
      else if (currentPicture.compare("Foto 2") == 0) {
          file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(4).toString()));
        } else {
          file.setFileName(m_bLayer.relativePath().append("/" + m_dataList_monitoreo.value(5).toString()));
        }
      //        }
      if (!file.copy(fileName)) {
          QMessageBox::critical(this, qApp->applicationName(),"Error al guardar el archivo");
          return;
        }
      QMessageBox::information(this, qApp->applicationName(),"Archivo guardado");
    });
}

void FrmPrincipal::loadCboTemas() noexcept{
  m_cboTemas = new QComboBox(this);
  m_cboTemas->addItem(QIcon(":/img/whitetheme.png"), "Tema claro");
  m_cboTemas->addItem(QIcon(":/img/darktheme.png"),"Tema oscuro");
  //  ui->toolBar->addSeparator();
  ui->toolBar->insertWidget(ui->actionAcerca_de, m_cboTemas);

  QObject::connect(m_cboTemas, QOverload<int>::of(&QComboBox::activated), this, [&]() {
      m_cboTemas->currentIndex() == 0
          ? QEasySettings::setStyle(QEasySettings::Style::lightFusion)
          : QEasySettings::setStyle(QEasySettings::Style::darkFusion);
    });
  ui->lblfoto->setScaledContents(true);
  ui->lblfoto->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  //  delete m_cboTemas;
}

void FrmPrincipal::haveData() noexcept{
  m_dataList.isEmpty()
      ? ui->actionEditar_datos_grupo_o_consorcio_minero->setDisabled(true)
      : ui->actionEditar_datos_grupo_o_consorcio_minero->setEnabled(true);
  m_dataList.isEmpty() ? ui->toolButton_2->setDisabled(true)
                       : ui->toolButton_2->setEnabled(true);
  m_dataListCliente.isEmpty()
      ? ui->actionEditar_datos_unidad_minera->setDisabled(true)
      : ui->actionEditar_datos_unidad_minera->setEnabled(true);
  m_dataListCliente.isEmpty()
      ? ui->actionNuevo_punto_de_monitoreo->setDisabled(true)
      : ui->actionNuevo_punto_de_monitoreo->setEnabled(true);
  ui->lwEstaciones->count() == 0
      ? ui->actioneditar_datos_monitoreo->setDisabled(true)
      : ui->actioneditar_datos_monitoreo->setEnabled(true);
}

void FrmPrincipal::readSettings() noexcept{
  QEasySettings::init(QEasySettings::Format::regFormat,qApp->organizationName());
  restoreGeometry(QEasySettings::readSettings("MainWindow", "geometry").toByteArray());
  restoreState(QEasySettings::readSettings("MainWindow", "windowState").toByteArray());
  ui->splitter->restoreState(QEasySettings::readSettings("MainWindow", "splitterState").toByteArray());

  QEasySettings::setStyle(QEasySettings::readStyle());
  if (QEasySettings::readStyle() == QEasySettings::Style::lightFusion)
    m_cboTemas->setCurrentIndex(0);
  else
    m_cboTemas->setCurrentIndex(1);
}

void FrmPrincipal::writeSettings() noexcept{
  QEasySettings::writeSettings("MainWindow", "geometry", saveGeometry());
  QEasySettings::writeSettings("MainWindow", "windowState", saveState());
  QEasySettings::writeSettings("MainWindow", "splitterState", ui->splitter->saveState());

  if (m_cboTemas->currentText().compare("Tema claro") == 0)
    QEasySettings::writeStyle(QEasySettings::Style::lightFusion);
  else
    QEasySettings::writeStyle(QEasySettings::Style::darkFusion);
}

void FrmPrincipal::datosMonitoreo() noexcept{

  m_dataList_monitoreo = m_bLayer.dataEstMonitoreo(m_dataTableRef.key(ui->cboReferencia->currentText()),
                                                   m_dataList_2.key(ui->lwEstaciones->currentItem()->data(
                                                                      Qt::DisplayRole).toString()));
  ui->txtEstacion->setText(m_dataList_monitoreo.value(0).toString());
  ui->deFecha->setDate(m_dataList_monitoreo.value(1).toDate());
  ui->teHora->setTime(m_dataList_monitoreo.value(2).toTime());
  //  ui->txtDescripcion->setPlainText(m_dataList_monitoreo.value(6).toString());
  ui->lwFotos->clear();
  loadPictures();
  // coordenas}
  ui->dsbEste->setValue(m_dataList_monitoreo.value(11).toDouble());
  ui->dsbNorte->setValue(m_dataList_monitoreo.value(12).toDouble());
  ui->dsbCota->setValue(m_dataList_monitoreo.value(13).toDouble());
  // descripcion del punto
  ui->txtDesc_punto->setText(m_dataList_monitoreo.value(6).toString());
  //  ui->txtDesc_punto->setToolTip(m_dataList_monitoreo.value(6).toString());

  // datos de campo
  ui->dsbPh->setValue(m_dataList_monitoreo.value(7).toDouble());
  ui->dsbTemp->setValue(m_dataList_monitoreo.value(8).toDouble());
  ui->dsbOd->setValue(m_dataList_monitoreo.value(9).toDouble());
  ui->dsbCond->setValue(m_dataList_monitoreo.value(10).toDouble());
}


void FrmPrincipal::cleanControls() noexcept{
  ui->txtEstacion->clear();
  ui->deFecha->setDate(QDate::currentDate());
  ui->teHora->setTime(QTime::currentTime());
  //  ui->txtDescripcion->clear();
  ui->lwEstaciones->clear();
  ui->lwFotos->clear();
  ui->dsbPh->setValue(0);
  ui->dsbTemp->setValue(0);
  ui->dsbOd->setValue(0);
  ui->dsbCond->setValue(0);
  ui->dsbEste->setValue(0);
  ui->dsbNorte->setValue(0);
  ui->dsbCota->setValue(0);
  ui->txtDesc_punto->clear();
  //  defaultImage();
}

void FrmPrincipal::defaultImage() noexcept {
  if (ui->lwEstaciones->count() == 0) {
      QPixmap defaultImage(":/img/default.png");
      ui->lblfoto->setPixmap(defaultImage);
    }
}

QImage FrmPrincipal::openPicture(const QString &f) noexcept{
  QFile file(m_bLayer.relativePath().append("/" + f));
  if (!file.open(QFile::ReadOnly)) {
      return QImage();
    }
  return QImage (file.fileName());
  //  return ;
}

void FrmPrincipal::showFotoToControl(const QListWidgetItem *item) noexcept{
  //  QPixmap pixMap;
  if(!item)
    return;
  if (item->data(Qt::DisplayRole).toString() == "Foto 1") {
      //    pixMap.loadFromData(m_foto_1);
      ui->lblfoto->setPixmap(QPixmap::fromImage(m_foto_1));
      ui->actionGuardar_foto->setEnabled(true);
      ui->actionGuardar_foto_como->setEnabled(true);
    }
  if (item->data(Qt::DisplayRole).toString() == "Foto 2") {
      //    pixMap.loadFromData(m_foto_2);
      ui->lblfoto->setPixmap(QPixmap::fromImage(m_foto_2));
      ui->actionGuardar_foto->setEnabled(true);
      ui->actionGuardar_foto_como->setEnabled(true);
    }
  if (item->data(Qt::DisplayRole).toString() == "Foto 3") {
      //    pixMap.loadFromData(m_foto_3);
      ui->lblfoto->setPixmap(QPixmap::fromImage(m_foto_3));
      ui->actionGuardar_foto->setEnabled(true);
      ui->actionGuardar_foto_como->setEnabled(true);
    }
}

void FrmPrincipal::tableProcedencia() noexcept{

  ui->lwProcedencia->clear();
  m_dataTableProc =m_bLayer.selectDataProc(m_dataListCliente.key(ui->cboUnidad->currentText()));
  //  ui->lwProcedencia->addItems(m_dataTableProc.values());

  if (!m_dataTableProc.isEmpty()) {
      QStringList l = m_dataTableProc.values();

      for (const auto& i:l) {
          QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui-11-128.png"),i, ui->lwProcedencia);
          ui->lwProcedencia->addItem(item);
        }
      ui->lwProcedencia->setCurrentItem(ui->lwProcedencia->item(indexRnd(ui->lwProcedencia->count() - 1)),
                                        QItemSelectionModel::Select);
    }

}

void FrmPrincipal::tableReferencia() noexcept{
  ui->cboReferencia->clear();
  //  unsigned int itemId = m_dataTableProc.key(ui->lwProcedencia->currentItem()->data(Qt::DisplayRole).toString());
  m_dataTableRef = m_bLayer.selectDataRef(m_dataTableProc.key(ui->lwProcedencia->currentItem()->data(
                                                                Qt::DisplayRole).toString()));
  if (!m_dataTableRef.isEmpty()) {
      ui->cboReferencia->addItems(m_dataTableRef.values());
    }

}

unsigned int FrmPrincipal::indexRnd(int limit)const noexcept{
  std::uniform_int_distribution dist(0, limit);
  return dist(*QRandomGenerator::global());
}

void FrmPrincipal::loadPictures() noexcept
{
  if (!m_dataList_monitoreo.isEmpty()) {
      //      QListWidgetItem *item{nullptr};
      if (!m_dataList_monitoreo.value(3).toString().isEmpty()) {
          QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui_-42-128.png"), "Foto 1", ui->lwFotos);
          ui->lwFotos->addItem(item);
          m_foto_1 = openPicture(m_dataList_monitoreo.value(3).toString());
        }
      if (!m_dataList_monitoreo.value(4).toString().isEmpty()) {
          QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui_-42-128.png"), "Foto 2",ui->lwFotos);
          ui->lwFotos->addItem(item);
          m_foto_2 = openPicture(m_dataList_monitoreo.value(4).toString());
        }

      if (!m_dataList_monitoreo.value(5).toString().isEmpty()) {
          QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui_-42-128.png"), "Foto 3",ui->lwFotos);
          ui->lwFotos->addItem(item);
          m_foto_3 = openPicture(m_dataList_monitoreo.value(5).toString());
        }

      ui->lwFotos->setCurrentItem(ui->lwFotos->item(indexRnd(ui->lwFotos->count() - 1)),QItemSelectionModel::Select);
    }
}


QPaintEngine *FrmPrincipal::paintEngine() const {
  return QWidget::paintEngine();
}

void FrmPrincipal::closeEvent(QCloseEvent *event) {
  writeSettings();
  QMainWindow::closeEvent(event);
}

void FrmPrincipal::on_toolButton_clicked() {
  NuevoDialog nuevoFrm(NuevoDialog::DialogMode::GRUPO, QVariantList(), this);
  nuevoFrm.setWindowTitle(tr("Nuevo consorcio o grupo minero"));
  if (nuevoFrm.exec() == QDialog::Accepted) {
      ui->cboGrupo->clear();
      loadGpoMinerolist();
      haveData();
    }
}

void FrmPrincipal::on_toolButton_2_clicked() {
  NuevoDialog nuevoFrm(NuevoDialog::DialogMode::UNIDAD, QVariantList(), this);
  nuevoFrm.setWindowTitle(tr("Nueva unidad minera"));
  if (nuevoFrm.exec() == QDialog::Accepted) {
      ui->cboUnidad->clear();
      loadDataListCliente();
      loadDataEstMonitoreo();
    }
}

void FrmPrincipal::on_cboGrupo_activated(int index) {
  Q_UNUSED(index)
  loadDataListCliente();
  tableProcedencia();

  if (ui->lwProcedencia->model()->rowCount() == 0) {
      cleanControls();
      ui->actionGuardar_foto->setDisabled(true);
      ui->actionGuardar_foto_como->setDisabled(true);
      defaultImage();
    }

  haveData();
}

void FrmPrincipal::on_actionNuevo_punto_de_monitoreo_triggered() {
  NuevaEstMonitDialog nuevaEstFrm(this);
  nuevaEstFrm.setWindowTitle(qApp->applicationName().append(" - Nuevo"));
  if (nuevaEstFrm.exec() == QDialog::Accepted) {
      //      ui->lwEstaciones->clear();
      ui->cboGrupo->clear();
      loadGpoMinerolist();
      tableReferencia();
      loadDataEstMonitoreo();
      haveData();
    }
}

void FrmPrincipal::on_cboUnidad_activated(int index) {
  Q_UNUSED(index)

  tableProcedencia();

  if (ui->lwEstaciones->count() == 0) {
      cleanControls();
      ui->actionGuardar_foto->setDisabled(true);
      ui->actionGuardar_foto_como->setDisabled(true);
      defaultImage();
    }
  haveData();
}

void FrmPrincipal::on_actionEditar_datos_grupo_o_consorcio_minero_triggered() {

  NuevoDialog dlgEditGrupo(NuevoDialog::DialogMode::UPDATE_GPO,
                           m_bLayer.selectData(m_dataList.key(ui->cboGrupo->currentText()),
                                               BussinesLayer::Table::GPO_MINERO), this);
  dlgEditGrupo.setWindowTitle("Editar datos - Grupo o compañía minera");
  dlgEditGrupo.exec();
}

void FrmPrincipal::on_actionEditar_datos_unidad_minera_triggered() {

  NuevoDialog dlgEditUnidad(NuevoDialog::DialogMode::UPDATE_UNIDAD,
                            m_bLayer.selectData(m_dataListCliente.key(ui->cboUnidad->currentText()),
                                                BussinesLayer::Table::CLIENTE), this);
  dlgEditUnidad.setWindowTitle("Editar datos - Unidad minera");
  if (dlgEditUnidad.exec() == QDialog::Accepted) {
      loadDataListCliente();
    }
}

void FrmPrincipal::on_actioneditar_datos_monitoreo_triggered() {
  EditDataDialog editDialog(this);
  editDialog.setWindowTitle(qApp->applicationName().append(" - Editar información"));

  if (editDialog.exec() == QDialog::Accepted) {

      loadDataEstMonitoreo();
      haveData();
    }
}

void FrmPrincipal::on_lwEstaciones_itemSelectionChanged() {

  datosMonitoreo();
}

void FrmPrincipal::on_lwFotos_itemSelectionChanged() {
  if (m_dataList_monitoreo.value(3).toString().compare("\"Imagen por defeccto!\"") == 0) {
      ui->lblfoto->setPixmap(QPixmap(":/img/default.png"));
      return;
    }
  showFotoToControl(ui->lwFotos->currentItem());
}

void FrmPrincipal::on_lwProcedencia_itemSelectionChanged() {
  tableReferencia();
  loadDataEstMonitoreo();
  haveData();
  defaultImage();
}

void FrmPrincipal::on_cboReferencia_activated(int index) {
  Q_UNUSED(index)
  loadDataEstMonitoreo();
  haveData();
  defaultImage();
}

void FrmPrincipal::on_actionAcerca_de_triggered() {
  frmAcercaDe acercaDe(m_cboTemas->currentIndex(), this);
  acercaDe.setWindowTitle(qApp->applicationName().append(" - Acerca de"));
  acercaDe.exec();
}

bool FrmPrincipal::eventFilter(QObject *watched, QEvent *event) {
  if (event->type() == QEvent::ContextMenu && watched == ui->FrmPrincipal::centralwidget) {
      if (ui->toolBar->isHidden()) {
          QMenu *menu = new QMenu(this);
          menu->addAction(QIcon(":/img/showtoolbar.png"), "Mostrar tool bar",[&](){
              ui->toolBar->setVisible(true); });
          QContextMenuEvent *cEvent = static_cast<QContextMenuEvent *>(event);
          menu->exec(cEvent->globalPos());
        }
      return true;
    }
  return QWidget::eventFilter(watched, event);
}

void FrmPrincipal::on_actionSalir_triggered() { this->close(); }
