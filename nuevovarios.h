#pragma once

#include <QDialog>
#include "bussineslayer.h"

namespace Ui {
class NuevoVarios;
}

class NuevoVarios : public QDialog
{
  Q_OBJECT

public:
  explicit NuevoVarios(QWidget *parent = nullptr);
  ~NuevoVarios();

private slots:
  void on_btnAceptar_clicked();
  void on_btnCancelar_clicked();

private:
  Ui::NuevoVarios *ui;
  BussinesLayer m_bLayer{};
  QHash<unsigned int,QString> m_dataClient{};

  bool insertTablaMonitoreo();
  void loadDataClient();
};


