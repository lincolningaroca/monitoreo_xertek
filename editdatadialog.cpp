#include "editdatadialog.h"
#include "qeasysettings.hpp"
#include "ui_editdatadialog.h"

#include "desc_pdialog.h"
#include "editarefproc.h"
#include "fotodialog.h"
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QSqlQueryModel>

EditDataDialog::EditDataDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::EditDataDialog) {
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  loadGpoMinerolist();
  fillCboMatriz();
  fillCboNComparacion();
  datosMonitoreo();

  setupFotoDialog();
  setUpToolBtnClear();

  ui->lblMatriz->setStyleSheet("color:#007af4;");

  // conectar los tool buttons btnEditaRef y btnEditaProc
  connect(ui->btnEditaProc, &QToolButton::clicked, this,[&]() {
      QVariantList param{};
      param.append(ui->lwProcedencia->currentItem()->data(Qt::DisplayRole).toString());
      param.append(m_dataListCliente.key(ui->cboUnidad->currentText()));
      param.append(m_dataTableProc.key(ui->lwProcedencia->currentItem()->data(Qt::DisplayRole).toString()));
      EditaRefProc edit(1, param, this);
      if (edit.exec() == QDialog::Accepted)
        tableProcedencia();
    });
  connect(ui->btnEditaref, &QToolButton::clicked, this,[&]() {
      QVariantList param{};
      param.append(ui->cboReferencia->currentText());
      param.append(m_dataTableProc.key(ui->lwProcedencia->currentItem()->data(Qt::DisplayRole).toString()));
      param.append(ui->cboMatriz->currentText());
      param.append(ui->cboNorma->currentText());
      param.append(m_matrizList.value(ui->cboMatriz->currentText()));
      param.append(m_dataTableRef.key(ui->cboReferencia->currentText()));
      EditaRefProc edit(2, param, this);
      if (edit.exec() == QDialog::Accepted)
        tableReferencia();
    });
  readSettings();
}

EditDataDialog::~EditDataDialog() { delete ui;}

void EditDataDialog::loadGpoMinerolist() noexcept{
  m_dataList = m_bLayer.gpoMineroList(BussinesLayer::Table::GPO_MINERO);
  if (!m_dataList.isEmpty()) {
      ui->cboGrupo->addItems(m_dataList.values());
    }
  loadDataListCliente();
}

void EditDataDialog::loadDataListCliente() noexcept{

  ui->cboUnidad->clear();
  m_dataListCliente = m_bLayer.gpoMineroList(BussinesLayer::Table::CLIENTE,
                                         m_dataList.key(ui->cboGrupo->currentText()));
  if (!m_dataListCliente.isEmpty()) {
      manageControls(2);
      ui->cboUnidad->addItems(m_dataListCliente.values());
    } else
    manageControls(1);
  tableProcedencia();
}

void EditDataDialog::datosMonitoreo() noexcept{

  if(m_model){
      m_list = m_bLayer.dataEstMonitoreo(m_dataTableRef.key(ui->cboReferencia->currentText()),
                                     m_model->index(ui->twEstaciones->currentIndex().row(),
                                                    0).data().toInt());
    }
  if (m_list.isEmpty()) {
      //    QMessageBox::information(this,qApp->applicationName(),"No hay datos");
      return;
    }
  ui->lineEdit->setText(m_list.value(0).toString());
  ui->dateEdit->setDate(m_list.value(1).toDate());
  ui->timeEdit->setTime(m_list.value(2).toTime());
  //  ui->plainTextEdit->setPlainText(list.value(3).toString());

  if (!m_list.value(3).toString().isEmpty()) {
      ui->lineEdit_2->setText("Foto 1");
      m_imagen_1 = openPicture(m_bLayer.relativePath().append("/" + m_list.value(3).toString()));
      //    oldPath_1=list.value(4).toString();
    }
  //  else
  //    ui->lineEdit_2->clear();
  if (!m_list.value(4).toString().isEmpty()) {
      ui->lineEdit_3->setText("Foto 2");
      m_imagen_2 = openPicture(m_bLayer.relativePath().append("/" + m_list.value(4).toString()));
      //    oldPath_2=list.value(5).toString();
    } else
    ui->lineEdit_3->clear();
  if (!m_list.value(5).toString().isEmpty()) {
      ui->lineEdit_4->setText("Foto 3");
      m_imagen_3 = openPicture(m_bLayer.relativePath().append("/" + m_list.value(5).toString()));

      //    oldPath_3=list.value(6).toString();
    } else
    ui->lineEdit_4->clear();
  m_desc_punto = m_list.value(6).toString();

  ui->dsbPh->setValue(m_list.value(7).toDouble());
  ui->dsbTemp->setValue(m_list.value(8).toDouble());
  ui->dsbOd->setValue(m_list.value(9).toDouble());
  ui->dsbCond->setValue(m_list.value(10).toDouble());
  ui->dsbEste->setValue(m_list.value(11).toDouble());
  ui->dsbNorte->setValue(m_list.value(12).toDouble());
  ui->dsbCota->setValue(m_list.value(13).toDouble());
  ui->cboMatriz->setCurrentText(m_list.value(15).toString());
  ui->lblMatriz->setText(m_matrizList.value(ui->cboMatriz->currentText()));
  ui->cboNorma->setCurrentText(m_list.value(16).toString());
}

void EditDataDialog::on_btnCancelar_clicked() { accept(); }

void EditDataDialog::on_cboGrupo_activated(int index) {
  Q_UNUSED(index)
  //  ui->cboUnidad->clear();
  loadDataListCliente();
  dataModel();
  //  setUpTableView();
}

void EditDataDialog::on_cboUnidad_activated(int index) {
  Q_UNUSED(index);
  tableProcedencia();
  //  model->clear();
  //  cleanData();
  dataModel();
  //  setUpTableView();
}
void EditDataDialog::dataModel() noexcept{
  m_model = new QSqlQueryModel(this);
  m_model = m_bLayer.selectCodEstacion_q(m_dataTableRef.key(ui->cboReferencia->currentText()));

  if (!m_model) {
      manageControls(1);
    } else {
      manageControls(2);
    }

  ui->twEstaciones->setModel(m_model);
  QItemSelectionModel *selectionModel = ui->twEstaciones->selectionModel();
  selectionModel->select(ui->twEstaciones->model()->index(0, 0), QItemSelectionModel::Select);
  setUpTableView();
  datosMonitoreo();

  QObject::connect(ui->twEstaciones->selectionModel(), &QItemSelectionModel::currentChanged,
                   this, &EditDataDialog::datosMonitoreo);
}

void EditDataDialog::manageControls(const int &op) noexcept {
  if (op == 1) {
      //    ui->twEstaciones->setColumnCount(0);
      ui->twEstaciones->setDisabled(true);
      ui->lineEdit->setDisabled(true);
      ui->dateEdit->setDisabled(true);
      ui->timeEdit->setDisabled(true);
      //    ui->plainTextEdit->setDisabled(true);
      ui->groupBox_2->setDisabled(true);
      ui->groupBox_4->setDisabled(true);
      ui->groupBox_5->setDisabled(true);
      ui->btnEliminar->setDisabled(true);
      ui->btnGuardar->setDisabled(true);
      ui->label_3->setDisabled(true);
      ui->label_4->setDisabled(true);
      ui->label_5->setDisabled(true);
      ui->cboMatriz->setDisabled(true);
      //    ui->label_16->setDisabled(true);

    } else {
      ui->twEstaciones->setEnabled(true);
      ui->lineEdit->setEnabled(true);
      ui->dateEdit->setEnabled(true);
      ui->timeEdit->setEnabled(true);
      //    ui->plainTextEdit->setEnabled(true);
      ui->groupBox_2->setEnabled(true);
      ui->groupBox_4->setEnabled(true);
      ui->groupBox_5->setEnabled(true);
      ui->btnGuardar->setEnabled(true);
      ui->btnEliminar->setEnabled(true);
      ui->label_3->setEnabled(true);
      ui->label_4->setEnabled(true);
      ui->label_5->setEnabled(true);
      ui->cboMatriz->setEnabled(true);
      //    ui->label_16->setEnabled(true);
    }
}

QImage EditDataDialog::openPicture(const QString &f) const noexcept {
  QFile file(f);
  if (!file.open(QFile::ReadOnly)) {
      return QImage{};
    }
 return  QImage {file.fileName()};

}

void EditDataDialog::setUpTableView() noexcept {
  if(m_model){
      m_model->setHeaderData(1, Qt::Horizontal, "CODIGO DE ESTACION");
      m_model->setHeaderData(2, Qt::Horizontal, "FECHA");

      ui->twEstaciones->hideColumn(0);
      ui->twEstaciones->hideColumn(3);
      ui->twEstaciones->setColumnWidth(1, 150);
      if (m_model->rowCount() != 0)
        ui->twEstaciones->selectRow(0);
    }
}
void EditDataDialog::on_btnEliminar_clicked() {
//  QVariant idEstacion = m_model->index(ui->twEstaciones->currentIndex().row(), 0).data();
//  QString codEstacion = m_list.value(0).toString();

  QMessageBox msgBox(this);
  msgBox.setText(QString("<p>Seguro que desea eliminar este registro:%1</p>")
        .arg("<p style=\"color:#CF1F25;\"><b>" + m_list.value(0).toString() + "</b></p>\n") +
        "Recuerde que al eliminar un registro, tambien se eliminaran todos los "
        "datos que tenga asociado. 'Inluyendo las imagenes.'");
  msgBox.setIcon(QMessageBox::Question);
  msgBox.addButton(new QPushButton("Eliminar registro",this),QMessageBox::AcceptRole);
  msgBox.addButton(new QPushButton("Cancelar",this), QMessageBox::RejectRole);
  if (msgBox.exec() == QMessageBox::RejectRole)
    return;

  QVariantList param;
  param.append(m_model->index(ui->twEstaciones->currentIndex().row(), 0).data());

  if (!m_bLayer.monitoreoMineroAction(param, BussinesLayer::TypeStm::DELETE)) {
      QMessageBox::critical(this, qApp->applicationName(),"Error al eliminar los datos.\n" +
                            m_bLayer.errorMessage() + "\n" +m_bLayer.errorCode());
      return;
    }
  if (!m_list.value(3).toString().isEmpty())
    QFile::remove(m_bLayer.relativePath().append("/" + m_list.value(3).toString()));
  if (!m_list.value(4).toString().isEmpty())
    QFile::remove(m_bLayer.relativePath().append("/" + m_list.value(4).toString()));
  if (!m_list.value(5).toString().isEmpty())
    QFile::remove(m_bLayer.relativePath().append("/" + m_list.value(5).toString()));

  QMessageBox::information(this, qApp->applicationName(), "Registro eliminado");
  //  accept();
  //    cleanData();
  dataModel();
  if (m_model->rowCount() != 0)
    datosMonitoreo();
}

void EditDataDialog::on_btnGuardar_clicked() {
  QVariantList datos{};
  datos.append(ui->lineEdit->text().toUpper());
  datos.append(ui->dateEdit->date());
  datos.append(ui->timeEdit->time());
  //  datos.append(ui->plainTextEdit->toPlainText());
  if (!m_newPath_1.isEmpty()) {
      datos.append(m_newPath_1.mid(m_newPath_1.lastIndexOf("/") + 1));
      QFile::remove(m_bLayer.relativePath().append("/" + m_list.value(3).toString()));
    } else {
      datos.append(m_list.value(3).toString());
    }
  if (!m_newPath_2.isEmpty()) {
      if (!m_list.value(4).toString().isEmpty()) {
          datos.append(m_newPath_2.mid(m_newPath_2.lastIndexOf("/") + 1));
          QFile::remove(
                m_bLayer.relativePath().append("/" + m_list.value(4).toString()));
        } else
        datos.append(m_newPath_2.mid(m_newPath_2.lastIndexOf("/") + 1));
    } else {
      if (!ui->lineEdit_3->text().isEmpty())
        datos.append(m_list.value(4).toString());
      else {
          if (!m_list.value(4).toString().isEmpty()) {
              QFile::remove(
                    m_bLayer.relativePath().append("/" + m_list.value(4).toString()));
              datos.append(QString());
            } else
            datos.append(QString());
        }
    }
  if (!m_newPath_3.isEmpty()) {
      if (!m_list.value(5).toString().isEmpty()) {
          QFile::remove(
                m_bLayer.relativePath().append("/" + m_list.value(5).toString()));
          datos.append(m_newPath_3.mid(m_newPath_3.lastIndexOf("/") + 1));
        } else
        datos.append(m_newPath_3.mid(m_newPath_3.lastIndexOf("/") + 1));
    } else {
      if (!ui->lineEdit_4->text().isEmpty())
        datos.append(m_list.value(5).toString());
      else {
          if (!m_list.value(5).toString().isEmpty()) {
              QFile::remove(
                    m_bLayer.relativePath().append("/" + m_list.value(5).toString()));
              datos.append(QString());
            } else
            datos.append(QString());
        }
    }
  datos.append(m_desc_punto.toUpper());
  datos.append(ui->dsbPh->value());
  datos.append(ui->dsbTemp->value());
  datos.append(ui->dsbOd->value());
  datos.append(ui->dsbCond->value());
  //  datos.append(m_m_dataListCliente.key(ui->cboUnidad->currentText()));
  datos.append(ui->dsbEste->value());
  datos.append(ui->dsbNorte->value());
  datos.append(ui->dsbCota->value());
  datos.append(m_model->index(ui->twEstaciones->currentIndex().row(), 0).data().toInt());

  if (ui->lineEdit->text().simplified().isEmpty()) {
      QMessageBox::warning(this, qApp->applicationName(),"El campo código de estación es requerido.");
      ui->lineEdit->selectAll();
      ui->lineEdit->setFocus(Qt::OtherFocusReason);
      return;
    }

  if (!m_bLayer.monitoreoMineroAction(datos, BussinesLayer::TypeStm::UPDATE)) {
      QMessageBox::critical(this, qApp->applicationName(),"Error al actualizar los datos.\n" +
                            m_bLayer.errorMessage() + "\n" +m_bLayer.errorCode());
      return;
    }

  if (!m_newPath_1.isEmpty()) {
      QFile::copy(m_pathImgOriginal_1, m_newPath_1);
    }
  if (!m_newPath_2.isEmpty()) {
      QFile::copy(m_pathImgOriginal_2, m_newPath_2);
    }
  if (!m_newPath_3.isEmpty()) {
      QFile::copy(m_pathImgOriginal_3, m_newPath_3);
    }
  // guardar en la tabla referencias
  QVariantList param{};
  param.append(ui->cboMatriz->currentText());
  param.append(ui->cboNorma->currentText());
  param.append(m_matrizList.value(ui->cboMatriz->currentText()));
  param.append(m_dataTableRef.key(ui->cboReferencia->currentText()));

  if (!m_bLayer.updateReferencias(param)) {
      QMessageBox::critical(this, qApp->applicationName(),"Error al actualizar los datos, tabla referencias.\n" +
                            m_bLayer.errorMessage() + "\n" + m_bLayer.errorCode());
      return;
    }
  QMessageBox::information(this, qApp->applicationName(), "Datos guardados.");
  m_newPath_1.clear();
  m_newPath_2.clear();
  m_newPath_3.clear();
  //  accept();
  //  cleanData();
  dataModel();
  datosMonitoreo();

  //  qDebug()<<m_bLayer.nro(m_m_dataListCliente.key(ui->cboUnidad->currentText()));
}

void EditDataDialog::on_toolButton_clicked() {

  QString fileName=showDialogFile();
  if(fileExists(fileName)){
      QMessageBox::warning(this, qApp->applicationName(),tr(
                             "Ya existe una imagen con este nombre:\n %1").arg(
                             QFileInfo(fileName).fileName()));
      return;
    }
  setPath(fileName,m_newPath_1,m_pathImgOriginal_1);
  ui->lineEdit_2->setText(QFileInfo(fileName).fileName());
}
void EditDataDialog::setPath(QString& fileName, QString& nPath, QString& oPath) noexcept{
  QFileInfo info{fileName};
  m_pathToImage = info.absoluteDir().absolutePath();
  QString absolutePath;
  absolutePath.append(m_bLayer.relativePath());
  absolutePath.append("/");
  absolutePath.append(info.fileName());
  nPath = absolutePath;
  oPath = fileName;
}
void EditDataDialog::on_pushButton_clicked() {

  Desc_pDialog desc_dialog{2, m_list.value(6).toString(),this};

  if (desc_dialog.exec() == QDialog::Accepted)
    m_desc_punto = desc_dialog.desc();
}

void EditDataDialog::on_toolButton_2_clicked() {

  QString fileName=showDialogFile();
  if(fileExists(fileName)){
      QMessageBox::warning(this, qApp->applicationName(),
                           tr("Ya existe una imagen con este nombre:\n %1").arg(QFileInfo(fileName).fileName()));
      return;
    }
  setPath(fileName,m_newPath_2,m_pathImgOriginal_2);
  ui->lineEdit_3->setText(QFileInfo(fileName).fileName());
}

void EditDataDialog::on_toolButton_3_clicked() {

  QString fileName=showDialogFile();
  if(fileExists(fileName)){
      QMessageBox::warning(this, qApp->applicationName(),
                           tr("Ya existe una imagen con este nombre:\n %1").arg(QFileInfo(fileName).fileName()));
      return;
    }
  setPath(fileName,m_newPath_3,m_pathImgOriginal_3);
  ui->lineEdit_3->setText(QFileInfo(fileName).fileName());
}
// colocar botones de borrado en los textbox de las fotos
void EditDataDialog::setUpToolBtnClear() noexcept {
  QAction *closeAction = ui->lineEdit_2->addAction(QIcon(":/img/1916591.png"), QLineEdit::TrailingPosition);
  closeAction->setToolTip("Quitar foto");
  connect(closeAction, &QAction::triggered, this, [&]() {
      QMessageBox::warning(this, qApp->applicationName(),
                           "No puede quitar ésta imagen.\n"
                           "La primera imagen siempre es requerida.\n"
                           "Recuerde que tiene que guardar los datos con una imagen como mínimo.");
      return;
      //    ui->lineEdit_2->clear();
    });

  QAction *closeAction1 = ui->lineEdit_3->addAction(QIcon(":/img/1916591.png"),QLineEdit::TrailingPosition);
  closeAction1->setToolTip("Quitar foto");
  connect(closeAction1, &QAction::triggered, this, [&]() {
      if (ui->lineEdit_3->text().isEmpty()) {
          return;
        }
      QMessageBox mBox;
      mBox.setText("Seguro que desea quitar ésta imagen?");
      mBox.setIcon(QMessageBox::Question);
      mBox.addButton(new QPushButton("Quitar imagen"), QMessageBox::AcceptRole);
      mBox.addButton(new QPushButton("Cancelar"), QMessageBox::RejectRole);
      if (mBox.exec() == QMessageBox::RejectRole)
        return;
      if (!m_newPath_2.isEmpty())
        m_newPath_2.clear();
      ui->lineEdit_3->clear();
    });

  QAction *closeAction2 = ui->lineEdit_4->addAction(QIcon(":/img/1916591.png"), QLineEdit::TrailingPosition);
  closeAction2->setToolTip("Quitar foto");
  connect(closeAction2, &QAction::triggered, this, [&]() {
      if (ui->lineEdit_4->text().isEmpty()) {
          return;
        }
      QMessageBox mBox;
      mBox.setText("Seguro que desea quitar ésta imagen?");
      mBox.setIcon(QMessageBox::Question);
      mBox.addButton(new QPushButton("Quitar imagen"), QMessageBox::AcceptRole);
      mBox.addButton(new QPushButton("Cancelar"), QMessageBox::RejectRole);
      if (mBox.exec() == QMessageBox::RejectRole)
        return;
      if (!m_newPath_3.isEmpty())
        m_newPath_3.clear();
      ui->lineEdit_4->clear();
    });
}

void EditDataDialog::setupFotoDialog() noexcept{
  QObject::connect(ui->lineEdit_2, &SWCustomTxt::clicked, this, [&]() {
      const QString& name = m_list.value(3).toString();
      //    name=name.mid(name.lastIndexOf("/")+1);
      FotoDialog fDialog(name, m_imagen_1, this);
      fDialog.exec();
    });
  QObject::connect(ui->lineEdit_3, &SWCustomTxt::clicked, this, [&]() {
      const QString& name = m_list.value(4).toString();
      //    name=name.mid(name.lastIndexOf("/")+1);
      if (!m_list.value(4).toString().isEmpty() && !ui->lineEdit_3->text().isEmpty()) {
          FotoDialog fDialog(name, m_imagen_2, this);
          fDialog.exec();
        }
    });
  QObject::connect(ui->lineEdit_4, &SWCustomTxt::clicked, this, [&]() {
      const QString& name = m_list.value(5).toString();
      //    name=name.mid(name.lastIndexOf("/")+1);
      if (!m_list.value(5).toString().isEmpty() && !ui->lineEdit_4->text().isEmpty()) {
          FotoDialog fDialog(name, m_imagen_3, this);
          fDialog.exec();
        }
    });
}

void EditDataDialog::tableProcedencia() noexcept{
  if (ui->cboUnidad->count() == 0) {
      ui->lwProcedencia->clear();
      return;
    }
  ui->lwProcedencia->clear();
  m_dataTableProc =m_bLayer.selectDataProc(m_dataListCliente.key(ui->cboUnidad->currentText()));
  //  ui->lwProcedencia->addItems(m_dataTableProc.values());

  if (!m_dataTableProc.isEmpty()) {
      QStringList l = m_dataTableProc.values();

      for (int i = 0; i < m_dataTableProc.count(); ++i) {
          QListWidgetItem *item = new QListWidgetItem(QIcon(":/img/ui-11-128.png"), l.value(i),ui->lwProcedencia);
          ui->lwProcedencia->addItem(item);
        }
      ui->lwProcedencia->setCurrentItem(ui->lwProcedencia->item(0), QItemSelectionModel::Select);
    }
  tableReferencia();
}

void EditDataDialog::tableReferencia() noexcept{

  ui->cboReferencia->clear();
  m_dataTableRef = m_bLayer.selectDataRef(m_dataTableProc.key(ui->lwProcedencia->currentItem()->data(
                                                                Qt::DisplayRole).toString()));
  if (!m_dataTableRef.isEmpty())
    ui->cboReferencia->addItems(m_dataTableRef.values());
  dataModel();
  ui->lblMatriz->setText(m_matrizList.value(ui->cboMatriz->currentText()));
  //  fillCboMatriz();
}

void EditDataDialog::fillCboMatriz() noexcept{
  ui->cboMatriz->clear();
  m_matrizList = m_bLayer.matriz();
  ui->cboMatriz->addItems(m_matrizList.keys());
  ui->lblMatriz->setText(m_matrizList.value(ui->cboMatriz->currentText()));
}

void EditDataDialog::fillCboNComparacion() noexcept {
  ui->cboNorma->clear();
  ui->cboNorma->addItems(m_bLayer.nComparacionList());
}

QString EditDataDialog::showDialogFile() noexcept
{
  QFileDialog fDialog(this);
  fDialog.setWindowTitle("Cargar una foto o imagen");
  fDialog.setDirectory(m_pathToImage);
  fDialog.setNameFilter("Imagenes (*.jpg *.jpeg *.png *.bmp)");
  if (fDialog.exec() == QDialog::Rejected) {
      m_pathToImage = fDialog.directory().absolutePath();
      return QString{};
    }
  const QStringList& listFiles = fDialog.selectedFiles();
  return listFiles.first();
//  return fileName;

}

bool EditDataDialog::fileExists(const QString& fName) noexcept
{
  QFileInfo info(fName);
  return QFileInfo::exists(m_bLayer.relativePath().append("/").append(info.fileName()));

}

void EditDataDialog::writeSettings() const noexcept {
  QEasySettings::writeSettings("File Dialog", "dialogPath", m_pathToImage);
}

void EditDataDialog::readSettings() noexcept {
  QEasySettings::init(QEasySettings::Format::regFormat, qApp->organizationName());
  m_pathToImage = QEasySettings::readSettings("File Dialog", "dialogPath").toString();
}

QPaintEngine *EditDataDialog::paintEngine() const {
  return QWidget::paintEngine();
}

void EditDataDialog::closeEvent(QCloseEvent *event) {
  //  accept();
  writeSettings();
  event->accept();
}

void EditDataDialog::on_lwProcedencia_itemSelectionChanged() {
  tableReferencia();
}

void EditDataDialog::on_cboReferencia_activated(int index) {
  Q_UNUSED(index)
  dataModel();
}

void EditDataDialog::on_cboMatriz_activated(const QString &arg1) {

  ui->lblMatriz->setText(m_matrizList.value(arg1));
}
