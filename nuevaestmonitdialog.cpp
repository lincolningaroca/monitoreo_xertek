#include "nuevaestmonitdialog.h"
#include "ui_nuevaestmonitdialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QAction>
#include "desc_pdialog.h"
#include "nuevovarios.h"
#include "nuevodialog.h"
#include "referenciadialog.h"
#include "generareferenciadialog.h"
#include "qeasysettings.hpp"

NuevaEstMonitDialog::NuevaEstMonitDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::NuevaEstMonitDialog)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog|Qt::MSWindowsFixedSizeDialogHint);
  ui->deFecha->setDate(QDate::currentDate());
  ui->teHora->setTime(QTime::currentTime());
  loadGpoMinerolist();
  ui->btnFoto2->setDisabled(true);
  ui->btnFoto3->setDisabled(true);

  setUpToolBtnClear();
  contextMenuGenRef();
  readSettings();
}

NuevaEstMonitDialog::~NuevaEstMonitDialog()
{
  delete ui;
  //  qInfo()<<"Se destruyo nuevo dialog";
}

void NuevaEstMonitDialog::loadGpoMinerolist()noexcept
{
  m_dataList=m_bLayer.gpoMineroList(BussinesLayer::Table::GPO_MINERO);
  ui->cboGrupo->addItems(m_dataList.values());
  loadDataListCliente();
}

void NuevaEstMonitDialog::loadDataListCliente() noexcept
{
  m_dataListCliente=m_bLayer.gpoMineroList(BussinesLayer::Table::CLIENTE,m_dataList.key(ui->cboGrupo->currentText()));
  ui->cboUnidad->addItems(m_dataListCliente.values());
  loadDataProcedencia();
}

void NuevaEstMonitDialog::setUpToolBtnClear() noexcept
{
  QAction *closeAction=ui->txtPath1->addAction(QIcon(":/img/1916591.png"), QLineEdit::TrailingPosition);
  connect(closeAction,&QAction::triggered,this,[&](){
      ui->txtPath1->clear();
      ui->txtPath2->clear();
      ui->txtPath3->clear();
    });
  QAction *closeAction1=ui->txtPath2->addAction(QIcon(":/img/1916591.png"), QLineEdit::TrailingPosition);
  connect(closeAction1,&QAction::triggered,this,[&](){
      ui->txtPath2->clear();
      ui->txtPath3->clear();
      m_pathImage_2.clear();
    });
  QAction *closeAction2=ui->txtPath3->addAction(QIcon(":/img/1916591.png"),  QLineEdit::TrailingPosition);
  connect(closeAction2,&QAction::triggered,this,[&](){
      ui->txtPath3->clear();
      m_pathImage_3.clear();
    });

}

void NuevaEstMonitDialog::loadDataProcedencia() noexcept
{
  //  if(ui->lwProcedencia->model()->rowCount()!=0)
  ui->lwProcedencia->clear();
  m_dataProcedencia=m_bLayer.selectDataMonitoreo(m_dataListCliente.key(ui->cboUnidad->currentText()));

  if(!m_dataProcedencia.isEmpty()){
      QStringList l=m_dataProcedencia.values();
      for(const auto& i : l){
          QListWidgetItem *item=new QListWidgetItem(QIcon(":/img/ui-11-128.png"),i,ui->lwProcedencia);
          ui->lwProcedencia->addItem(item);

        }
      ui->lwProcedencia->setCurrentItem(ui->lwProcedencia->item(0),QItemSelectionModel::Select);
      //    loadDataReferencia()
    }
  loadDataReferencia();
}

void NuevaEstMonitDialog::loadDataReferencia() noexcept
{
  ui->cboReferencia->clear();
  m_dataReferencia=m_bLayer.selectDataRef(m_dataProcedencia.key(
                                            ui->lwProcedencia->currentItem()->data(
                                              Qt::DisplayRole).toString()));
  ui->cboReferencia->addItems(m_dataReferencia.values());

}


void NuevaEstMonitDialog::on_btnGuardar_clicked()
{
  if(ui->txtCodigo->text().simplified().isEmpty()){
      QMessageBox::warning(this,qApp->applicationName(),"El código de estación es requerido.\n");
      ui->txtCodigo->setFocus(Qt::OtherFocusReason);
      return;
    }
  if(ui->txtPath1->text().isEmpty()){
      QMessageBox::warning(this,qApp->applicationName(),"Debe ingresar al menos una foto.\n");
      ui->btnFoto1->setFocus(Qt::OtherFocusReason);
      return;
    }
  if(!m_bLayer.codeExists(ui->txtCodigo->text().simplified().toUpper(),
                         m_dataReferencia.key(ui->cboReferencia->currentText())) &&
     !m_bLayer.errorMessage().isEmpty()){
      QMessageBox::critical(this,qApp->applicationName(),"Error de ejecución\n"+m_bLayer.errorMessage());
      return;
    }

  if(m_bLayer.codeExists(ui->txtCodigo->text().simplified().toUpper(),
                         m_dataReferencia.key(ui->cboReferencia->currentText()))){
      QMessageBox::warning(this,qApp->applicationName(),
                           QString("El código de estación: %1").arg(
                             "<p style='font-weight:bold; color:#d83c66'>"+
                             ui->txtCodigo->text().toUpper()+"</p>"
                                                             "Ya esta registrado en esta referencia."));
      ui->txtCodigo->selectAll();
      ui->txtCodigo->setFocus(Qt::OtherFocusReason);
      return;
    }


  QVariantList param{};
  param.append(ui->txtCodigo->text().toUpper().simplified());
  param.append(ui->deFecha->date());
  param.append(ui->teHora->time());

  if(ui->checkBox->isChecked()){
      param.append(ui->txtPath1->text());
      param.append("");
      param.append("");
    }else{
      param.append(m_pathImage_1.mid(m_pathImage_1.lastIndexOf("/")+1));
      param.append(m_pathImage_2.mid(m_pathImage_2.lastIndexOf("/")+1));
      param.append(m_pathImage_3.mid(m_pathImage_3.lastIndexOf("/")+1));
    }

  param.append(m_desc_punto.simplified().toUpper());
  param.append(ui->dsbPh->value());
  param.append(ui->dsbTemp->value());
  param.append(ui->dsbOd->value());
  param.append(ui->dsbCond->value());
  //agregar las coordenadas
  param.append(ui->dsbEste->value());
  param.append(ui->dsbNorte->value());
  param.append(ui->dsbCota->value());


  if(!m_bLayer.monitoreoMineroAction(param,BussinesLayer::TypeStm::INSERT)){
      if(m_bLayer.errorCode()=="23503"){
          QMessageBox::warning(this,qApp->applicationName(),
                               "No existe algún registro donde se pueda almacenar esta información.\n\n"
                               "Esto se debe a que no existen datos en el campo, unidad minera.\n"
                               "* salga de ésta ventana y agregue una nueva unidad minera,"
                               "en el botón que se \n  muestra al lado de la lista desplegable"
                               " Unidad minera, del formulario principal.");
          ui->txtCodigo->selectAll();
          ui->txtCodigo->setFocus(Qt::OtherFocusReason);
          return;
        }

      QMessageBox::critical(this,qApp->applicationName(),m_bLayer.errorMessage());
      return;
    }
  //insertar datos en la tabla referencias_monitoreo
  if(ui->lwProcedencia->selectedItems().empty()){
      //    QMessageBox::critical(this,qApp->applicationName(),"Debe seleccionar algo");
      return;
    }
  if(ui->cboReferencia->model()->rowCount()==0){
      return;
    }

  QVariantList param_1{};
  param_1.append(m_dataReferencia.key(ui->cboReferencia->currentText()));
  param_1.append(m_bLayer.lastInsertId().toInt());
  if(!m_bLayer.tablaRefMonitoreo(param_1,BussinesLayer::TypeStm::INSERT)){
      QMessageBox::critical(this,qApp->applicationName(),m_bLayer.errorMessage());
      return;
    }

  if(!ui->checkBox->isChecked()){
      QFile::copy(m_oldPathimagen_1,m_pathImage_1);
      if(!m_pathImage_2.isEmpty())
        QFile::copy(m_oldPathimagen_2,m_pathImage_2);
      if(!m_pathImage_3.isEmpty())
        QFile::copy(m_oldPathimagen_3,m_pathImage_3);
    }

  QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
  //  qDebug()<<m_bLayer.lInsertId();
  writeSettings();
  accept();
}


void NuevaEstMonitDialog::on_btnFoto1_clicked()
{

  QFileDialog fDialog(this);
  fDialog.setWindowTitle("Cargar una foto o imagen");
  fDialog.setDirectory(m_pathToImage);
  fDialog.setNameFilter("Imagenes (*.jpg *.jpeg *.png *.bmp)");
  if(fDialog.exec()==QDialog::Rejected){
      m_pathToImage=fDialog.directory().absolutePath();
      return;
    }

  QStringList fileNames=fDialog.selectedFiles();
  QString& fileName=fileNames.first();
  QFileInfo info(fileName);

  if(info.fileName().compare(ui->txtPath2->text())==0
     ||info.fileName().compare(ui->txtPath3->text())==0 ){
      QMessageBox::warning(this,qApp->applicationName(),tr("La imágen, \"%1\"").arg(
                             info.fileName()).append("Ya fue agregada previamente."));
      return;

    }
  if(QFileInfo::exists(m_bLayer.relativePath().append("/").append(info.fileName()))){
      QMessageBox::warning(this,qApp->applicationName(),
                           tr("Ya existe una imagen con este nombre:\n %1").arg(info.fileName()));
      return;
    }
  m_pathToImage=info.absoluteDir().absolutePath();

  QString absolutePath{};
  absolutePath.append(m_bLayer.relativePath());
  absolutePath.append("/");
  absolutePath.append(info.fileName());
  m_pathImage_1=absolutePath;
  ui->txtPath1->setText(info.fileName());
  //  qDebug()<<pathImage_1.mid(pathImage_1.lastIndexOf("/")+1);
  m_oldPathimagen_1=fileName;

}


void NuevaEstMonitDialog::on_btnFoto2_clicked()
{
  QFileDialog fDialog(this);
  fDialog.setWindowTitle("Cargar una foto o imagen");
  fDialog.setDirectory(m_pathToImage);
  fDialog.setNameFilter("Imagenes (*.jpg *.jpeg *.png *.bmp)");
  if(fDialog.exec()==QDialog::Rejected){
      m_pathImage_2="";
      m_pathToImage=fDialog.directory().absolutePath();
      return;
    }
  QStringList fileNames=fDialog.selectedFiles();
  QString& fileName=fileNames.first();

  QFileInfo info(fileName);

  if(info.fileName().compare(ui->txtPath1->text())==0 ||
     info.fileName().compare(ui->txtPath3->text())==0 ){
      QMessageBox::warning(this,qApp->applicationName(),
                           tr("La imágen, \"%1\"").arg(
                             info.fileName()).append(" Ya fue agregada previamente."));
      return;

    }
  if(QFileInfo::exists(m_bLayer.relativePath().append("/").append(info.fileName()))){
      QMessageBox::warning(this,qApp->applicationName(),
                           tr("Ya existe una imagen con este nombre:\n %1").arg(info.fileName()));
      return;
    };
  m_pathToImage=info.absoluteDir().absolutePath();

  QString absolutePath{};
  absolutePath.append(m_bLayer.relativePath());
  absolutePath.append("/");
  absolutePath.append(info.fileName());
  m_pathImage_2=absolutePath;
  ui->txtPath2->setText(info.fileName());
  m_oldPathimagen_2=fileName;
}


void NuevaEstMonitDialog::on_btnFoto3_clicked()
{
  QFileDialog fDialog(this);
  fDialog.setWindowTitle("Cargar una foto o imagen");
  fDialog.setDirectory(m_pathToImage);
  fDialog.setNameFilter("Imagenes (*.jpg *.jpeg *.png *.bmp)");
  if(fDialog.exec()==QDialog::Rejected){
      m_pathImage_3="";
      m_pathToImage=fDialog.directory().absolutePath();
      return;
    }
  QStringList fileNames=fDialog.selectedFiles();
  QString& fileName=fileNames.first();

  QFileInfo info(fileName);

  if(info.fileName().compare(ui->txtPath1->text())==0 ||
     info.fileName().compare(ui->txtPath2->text())==0 ){
      QMessageBox::warning(this,qApp->applicationName(),tr("La imágen, \"%1\"").arg(
                             info.fileName()).append("Ya fue agregada previamente."));
      return;

    }
  if(QFileInfo::exists(m_bLayer.relativePath().append("/").append(info.fileName()))){
      QMessageBox::warning(this,qApp->applicationName(),
                           tr("Ya existe una imagen con este nombre:\n %1").arg(info.fileName()));
      return;
    }
  m_pathToImage=info.absoluteDir().absolutePath();

  QString absolutePath{};
  absolutePath.append(m_bLayer.relativePath());
  absolutePath.append("/");
  absolutePath.append(info.fileName());
  m_pathImage_3=absolutePath;
  ui->txtPath3->setText(info.fileName());
  m_oldPathimagen_3=fileName;
}



void NuevaEstMonitDialog::on_btnCancelar_clicked()
{
  writeSettings();
  reject();
}
void NuevaEstMonitDialog::on_txtPath1_textChanged(const QString &arg1)
{
  arg1.isEmpty() ? ui->btnFoto2->setDisabled(true) : ui->btnFoto2->setEnabled(true);
}

void NuevaEstMonitDialog::on_txtPath2_textChanged(const QString &arg1)
{
  arg1.isEmpty() ? ui->btnFoto3->setDisabled(true) : ui->btnFoto3->setEnabled(true);
}

void NuevaEstMonitDialog::on_pushButton_clicked()
{
  Desc_pDialog descDialog(2,m_desc_punto);
  if(descDialog.exec()==QDialog::Accepted)
    m_desc_punto=descDialog.desc();
}


void NuevaEstMonitDialog::on_btnProcedencia_clicked()
{
  NuevoVarios nVarios(this);
  nVarios.setWindowTitle(qApp->applicationName().append(" - Nueva procedencia"));
  if(nVarios.exec()==QDialog::Accepted){
      loadDataProcedencia();
    }
}


void NuevaEstMonitDialog::on_btnReferencia_clicked()
{
  ReferenciaDialog ref(this);
  ref.setWindowTitle(qApp->applicationName().append(" - Nueva referencia"));
  if(ref.exec()==QDialog::Accepted){
      loadDataProcedencia();
    }
}


void NuevaEstMonitDialog::on_lwProcedencia_itemSelectionChanged()
{
  ui->cboReferencia->clear();
  loadDataReferencia();
}


void NuevaEstMonitDialog::on_cboUnidad_activated(int index)
{
  Q_UNUSED(index)
  loadDataProcedencia();
}


void NuevaEstMonitDialog::on_cboGrupo_activated(int index)
{
  Q_UNUSED(index)
  ui->cboUnidad->clear();
  loadDataListCliente();
  loadDataReferencia();
}


void NuevaEstMonitDialog::on_btnNuevoGrupo_clicked()
{
  NuevoDialog nuevoGrupo(NuevoDialog::DialogMode::GRUPO);
  nuevoGrupo.setWindowTitle(tr("Nuevo consorcio o grupo minero"));
  if(nuevoGrupo.exec()==QDialog::Accepted){
      ui->cboUnidad->clear();
      loadGpoMinerolist();
    }
}


void NuevaEstMonitDialog::on_btnNuevaUnnidad_clicked()
{
  NuevoDialog nuevaUnidad(NuevoDialog::DialogMode::UNIDAD);
  nuevaUnidad.setWindowTitle(tr("Nueva unidad minera"));
  if(nuevaUnidad.exec()==QDialog::Accepted){
      ui->cboUnidad->clear();
      loadDataListCliente();
    }
}

void NuevaEstMonitDialog::writeSettings() noexcept
{
  QEasySettings::writeSettings("File Dialog","dialogPath",m_pathToImage);
}

void NuevaEstMonitDialog::readSettings() noexcept
{
  QEasySettings::init(QEasySettings::Format::regFormat,qApp->organizationName());
  m_pathToImage=QEasySettings::readSettings("File Dialog","dialogPath").toString();
}

void NuevaEstMonitDialog::contextMenuGenRef()
{
  ui->lwProcedencia->setContextMenuPolicy(Qt::ActionsContextMenu);
  QAction* genRefAction=new QAction("Generar referencia(s), apartir de una procedencia existente",this);
  ui->lwProcedencia->addAction(genRefAction);
  QObject::connect(genRefAction,&QAction::triggered,this,[&](){
      GeneraReferenciaDialog refDialog(m_dataListCliente.key(ui->cboUnidad->currentText()),
                                       m_dataProcedencia.key(ui->lwProcedencia->currentIndex().data().toString()), this);
      refDialog.setWindowTitle("Generar referencias");
      if(refDialog.exec()==QDialog::Accepted){
          ui->cboGrupo->clear();
          loadGpoMinerolist();
        }
    });
}


QPaintEngine *NuevaEstMonitDialog::paintEngine() const
{
  return QWidget::paintEngine();
}

void NuevaEstMonitDialog::closeEvent(QCloseEvent *event)
{
  writeSettings();
  QWidget::closeEvent(event);
}

void NuevaEstMonitDialog::on_checkBox_toggled(bool checked)
{
  if(checked){
      ui->groupBox_3->setDisabled(true);
      ui->txtPath1->setText("\"Imagen por defeccto!\"");
    }
  else{
      ui->groupBox_3->setEnabled(true);
      ui->txtPath1->clear();
    }
}

