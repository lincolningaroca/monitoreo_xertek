/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <swcustomtxt.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionActualizar_datos;
    QAction *actionEditar_datos_unidad_minera;
    QAction *actionNuevo_punto_de_monitoreo;
    QAction *actionayuda;
    QAction *actioneditar_datos_monitoreo;
    QAction *actionGuardar_foto;
    QAction *actionGuardar_foto_como;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *cboGrupo;
    QToolButton *toolButton;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QComboBox *cboUnidad;
    QToolButton *toolButton_2;
    QLabel *label_13;
    QListWidget *lwProcedencia;
    QLabel *label_14;
    QComboBox *cboReferencia;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_8;
    QListWidget *lwEstaciones;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *formLayout_5;
    QLabel *label_3;
    QLineEdit *txtEstacion;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QDateEdit *deFecha;
    QLabel *label_5;
    QTimeEdit *teHora;
    QLabel *label_6;
    QListWidget *lwFotos;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label_9;
    QDoubleSpinBox *dsbPh;
    QFormLayout *formLayout_2;
    QLabel *label_11;
    QDoubleSpinBox *dsbTemp;
    QFormLayout *formLayout_3;
    QLabel *label_10;
    QDoubleSpinBox *dsbOd;
    QFormLayout *formLayout_4;
    QLabel *label_12;
    QDoubleSpinBox *dsbCond;
    QGroupBox *groupBox_6;
    QHBoxLayout *horizontalLayout_7;
    QGridLayout *gridLayout_3;
    QLabel *label_15;
    QDoubleSpinBox *dsbEste;
    QLabel *label_16;
    QDoubleSpinBox *dsbNorte;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_17;
    QDoubleSpinBox *dsbCota;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_18;
    SWCustomTxt *txtDesc_punto;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *lblfoto;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(930, 659);
        actionActualizar_datos = new QAction(MainWindow);
        actionActualizar_datos->setObjectName(QString::fromUtf8("actionActualizar_datos"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/7985334.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionActualizar_datos->setIcon(icon);
        actionEditar_datos_unidad_minera = new QAction(MainWindow);
        actionEditar_datos_unidad_minera->setObjectName(QString::fromUtf8("actionEditar_datos_unidad_minera"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/img/7985337.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEditar_datos_unidad_minera->setIcon(icon1);
        actionNuevo_punto_de_monitoreo = new QAction(MainWindow);
        actionNuevo_punto_de_monitoreo->setObjectName(QString::fromUtf8("actionNuevo_punto_de_monitoreo"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/img/7985325.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNuevo_punto_de_monitoreo->setIcon(icon2);
        actionayuda = new QAction(MainWindow);
        actionayuda->setObjectName(QString::fromUtf8("actionayuda"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/img/114444.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionayuda->setIcon(icon3);
        actioneditar_datos_monitoreo = new QAction(MainWindow);
        actioneditar_datos_monitoreo->setObjectName(QString::fromUtf8("actioneditar_datos_monitoreo"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/img/7985334.png"), QSize(), QIcon::Normal, QIcon::Off);
        actioneditar_datos_monitoreo->setIcon(icon4);
        actionGuardar_foto = new QAction(MainWindow);
        actionGuardar_foto->setObjectName(QString::fromUtf8("actionGuardar_foto"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/img/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGuardar_foto->setIcon(icon5);
        actionGuardar_foto_como = new QAction(MainWindow);
        actionGuardar_foto_como->setObjectName(QString::fromUtf8("actionGuardar_foto_como"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/img/5684769.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGuardar_foto_como->setIcon(icon6);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout_3 = new QHBoxLayout(centralwidget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setMaximumSize(QSize(250, 16777215));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        cboGrupo = new QComboBox(groupBox);
        cboGrupo->setObjectName(QString::fromUtf8("cboGrupo"));

        horizontalLayout_2->addWidget(cboGrupo);

        toolButton = new QToolButton(groupBox);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));

        horizontalLayout_2->addWidget(toolButton);


        verticalLayout->addLayout(horizontalLayout_2);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cboUnidad = new QComboBox(groupBox);
        cboUnidad->setObjectName(QString::fromUtf8("cboUnidad"));

        horizontalLayout->addWidget(cboUnidad);

        toolButton_2 = new QToolButton(groupBox);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));

        horizontalLayout->addWidget(toolButton_2);


        verticalLayout->addLayout(horizontalLayout);

        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        verticalLayout->addWidget(label_13);

        lwProcedencia = new QListWidget(groupBox);
        lwProcedencia->setObjectName(QString::fromUtf8("lwProcedencia"));

        verticalLayout->addWidget(lwProcedencia);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        verticalLayout->addWidget(label_14);

        cboReferencia = new QComboBox(groupBox);
        cboReferencia->setObjectName(QString::fromUtf8("cboReferencia"));

        verticalLayout->addWidget(cboReferencia);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_4->addWidget(label_8);

        lwEstaciones = new QListWidget(groupBox);
        lwEstaciones->setObjectName(QString::fromUtf8("lwEstaciones"));

        verticalLayout_4->addWidget(lwEstaciones);


        verticalLayout->addLayout(verticalLayout_4);


        horizontalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        groupBox_2->setMinimumSize(QSize(350, 0));
        groupBox_2->setMaximumSize(QSize(280, 16777215));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_3);

        txtEstacion = new QLineEdit(groupBox_2);
        txtEstacion->setObjectName(QString::fromUtf8("txtEstacion"));
        txtEstacion->setReadOnly(true);

        formLayout_5->setWidget(0, QFormLayout::FieldRole, txtEstacion);


        verticalLayout_2->addLayout(formLayout_5);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 0, 0, 1, 1);

        deFecha = new QDateEdit(groupBox_2);
        deFecha->setObjectName(QString::fromUtf8("deFecha"));
        deFecha->setMinimumSize(QSize(110, 0));
        deFecha->setReadOnly(true);
        deFecha->setCalendarPopup(true);

        gridLayout_2->addWidget(deFecha, 0, 1, 1, 1);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 0, 2, 1, 1);

        teHora = new QTimeEdit(groupBox_2);
        teHora->setObjectName(QString::fromUtf8("teHora"));
        teHora->setMinimumSize(QSize(110, 0));
        teHora->setReadOnly(true);
        teHora->setCalendarPopup(false);

        gridLayout_2->addWidget(teHora, 0, 3, 1, 1);


        verticalLayout_2->addLayout(gridLayout_2);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_2->addWidget(label_6);

        lwFotos = new QListWidget(groupBox_2);
        lwFotos->setObjectName(QString::fromUtf8("lwFotos"));

        verticalLayout_2->addWidget(lwFotos);

        groupBox_4 = new QGroupBox(groupBox_2);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout = new QGridLayout(groupBox_4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_9);

        dsbPh = new QDoubleSpinBox(groupBox_4);
        dsbPh->setObjectName(QString::fromUtf8("dsbPh"));
        dsbPh->setReadOnly(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, dsbPh);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_11);

        dsbTemp = new QDoubleSpinBox(groupBox_4);
        dsbTemp->setObjectName(QString::fromUtf8("dsbTemp"));
        dsbTemp->setReadOnly(true);
        dsbTemp->setMaximum(999.990000000000009);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, dsbTemp);


        gridLayout->addLayout(formLayout_2, 0, 1, 1, 1);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_10);

        dsbOd = new QDoubleSpinBox(groupBox_4);
        dsbOd->setObjectName(QString::fromUtf8("dsbOd"));
        dsbOd->setReadOnly(true);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, dsbOd);


        gridLayout->addLayout(formLayout_3, 1, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_12);

        dsbCond = new QDoubleSpinBox(groupBox_4);
        dsbCond->setObjectName(QString::fromUtf8("dsbCond"));
        dsbCond->setReadOnly(true);
        dsbCond->setMaximum(9999999999.989999771118164);

        formLayout_4->setWidget(0, QFormLayout::FieldRole, dsbCond);


        gridLayout->addLayout(formLayout_4, 1, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_4);

        groupBox_6 = new QGroupBox(groupBox_2);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_15 = new QLabel(groupBox_6);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_3->addWidget(label_15, 0, 0, 1, 1);

        dsbEste = new QDoubleSpinBox(groupBox_6);
        dsbEste->setObjectName(QString::fromUtf8("dsbEste"));
        dsbEste->setMinimumSize(QSize(130, 0));
        dsbEste->setReadOnly(true);
        dsbEste->setMaximum(9999999999.000000000000000);

        gridLayout_3->addWidget(dsbEste, 0, 1, 1, 1);

        label_16 = new QLabel(groupBox_6);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_3->addWidget(label_16, 0, 2, 1, 1);

        dsbNorte = new QDoubleSpinBox(groupBox_6);
        dsbNorte->setObjectName(QString::fromUtf8("dsbNorte"));
        dsbNorte->setMinimumSize(QSize(130, 0));
        dsbNorte->setReadOnly(true);
        dsbNorte->setMaximum(9999999999.000000000000000);

        gridLayout_3->addWidget(dsbNorte, 0, 3, 1, 1);


        horizontalLayout_7->addLayout(gridLayout_3);


        verticalLayout_2->addWidget(groupBox_6);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_6->addWidget(label_17);

        dsbCota = new QDoubleSpinBox(groupBox_2);
        dsbCota->setObjectName(QString::fromUtf8("dsbCota"));
        dsbCota->setMaximumSize(QSize(170, 16777215));
        dsbCota->setReadOnly(true);
        dsbCota->setMaximum(9999999999.000000000000000);

        horizontalLayout_6->addWidget(dsbCota);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_4->addWidget(label_18);

        txtDesc_punto = new SWCustomTxt(groupBox_2);
        txtDesc_punto->setObjectName(QString::fromUtf8("txtDesc_punto"));
        txtDesc_punto->setReadOnly(true);

        horizontalLayout_4->addWidget(txtDesc_punto);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_3->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(centralwidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy1);
        groupBox_3->setMinimumSize(QSize(300, 0));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(2, 2, 2, 2);
        lblfoto = new QLabel(groupBox_3);
        lblfoto->setObjectName(QString::fromUtf8("lblfoto"));
        lblfoto->setScaledContents(true);

        verticalLayout_3->addWidget(lblfoto);


        horizontalLayout_3->addWidget(groupBox_3);

        MainWindow->setCentralWidget(centralwidget);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionActualizar_datos);
        toolBar->addAction(actionEditar_datos_unidad_minera);
        toolBar->addAction(actioneditar_datos_monitoreo);
        toolBar->addSeparator();
        toolBar->addAction(actionNuevo_punto_de_monitoreo);
        toolBar->addSeparator();
        toolBar->addAction(actionayuda);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionActualizar_datos->setText(QCoreApplication::translate("MainWindow", "Editar datos grupo o consorcio minero", nullptr));
#if QT_CONFIG(tooltip)
        actionActualizar_datos->setToolTip(QCoreApplication::translate("MainWindow", "Editar datos, grupo o consorcio minero (Shift+A)", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionActualizar_datos->setShortcut(QCoreApplication::translate("MainWindow", "Shift+A", nullptr));
#endif // QT_CONFIG(shortcut)
        actionEditar_datos_unidad_minera->setText(QCoreApplication::translate("MainWindow", "Editar datos unidad minera", nullptr));
#if QT_CONFIG(tooltip)
        actionEditar_datos_unidad_minera->setToolTip(QCoreApplication::translate("MainWindow", "Editar datos unidad minera (Shift+Q)", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionEditar_datos_unidad_minera->setShortcut(QCoreApplication::translate("MainWindow", "Shift+Q", nullptr));
#endif // QT_CONFIG(shortcut)
        actionNuevo_punto_de_monitoreo->setText(QCoreApplication::translate("MainWindow", "Nuevo punto de monitoreo", nullptr));
#if QT_CONFIG(tooltip)
        actionNuevo_punto_de_monitoreo->setToolTip(QCoreApplication::translate("MainWindow", "Nuevo punto de monitoreo (Ctrl+N)", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionNuevo_punto_de_monitoreo->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+N", nullptr));
#endif // QT_CONFIG(shortcut)
        actionayuda->setText(QCoreApplication::translate("MainWindow", "Acerca de", nullptr));
#if QT_CONFIG(tooltip)
        actionayuda->setToolTip(QCoreApplication::translate("MainWindow", "Acerca de", nullptr));
#endif // QT_CONFIG(tooltip)
        actioneditar_datos_monitoreo->setText(QCoreApplication::translate("MainWindow", "editar datos monitoreo", nullptr));
#if QT_CONFIG(tooltip)
        actioneditar_datos_monitoreo->setToolTip(QCoreApplication::translate("MainWindow", "Editar datos - estaci\303\263n de monitoreo (Ctrl+E)", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actioneditar_datos_monitoreo->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+E", nullptr));
#endif // QT_CONFIG(shortcut)
        actionGuardar_foto->setText(QCoreApplication::translate("MainWindow", "Guardar foto", nullptr));
        actionGuardar_foto_como->setText(QCoreApplication::translate("MainWindow", "Guardar foto como", nullptr));
        groupBox->setTitle(QString());
        label->setText(QCoreApplication::translate("MainWindow", "Grupo o compa\303\261\303\255a minera: ", nullptr));
#if QT_CONFIG(tooltip)
        toolButton->setToolTip(QCoreApplication::translate("MainWindow", "Nuevo grupo minero (Ctrl+Shift+A)", nullptr));
#endif // QT_CONFIG(tooltip)
        toolButton->setText(QCoreApplication::translate("MainWindow", "...", nullptr));
#if QT_CONFIG(shortcut)
        toolButton->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Shift+A", nullptr));
#endif // QT_CONFIG(shortcut)
        label_2->setText(QCoreApplication::translate("MainWindow", "Unidad minera: ", nullptr));
#if QT_CONFIG(tooltip)
        toolButton_2->setToolTip(QCoreApplication::translate("MainWindow", "Nueva unidad minera (Ctrl+Shift+S)", nullptr));
#endif // QT_CONFIG(tooltip)
        toolButton_2->setText(QCoreApplication::translate("MainWindow", "...", nullptr));
#if QT_CONFIG(shortcut)
        toolButton_2->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Shift+S", nullptr));
#endif // QT_CONFIG(shortcut)
        label_13->setText(QCoreApplication::translate("MainWindow", "Procedecia:", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Referencia:", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Puntos muestreados:", nullptr));
        groupBox_2->setTitle(QString());
        label_3->setText(QCoreApplication::translate("MainWindow", "C\303\263digo de estacion:", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Fecha:", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Hora:", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Cantidad de fotos:", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "Par\303\241metros de campo", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "PH:", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "Temp:", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "OD:", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "Cond:", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("MainWindow", "Coordenadas", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "E:", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "N:", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "Cota:", nullptr));
        dsbCota->setSuffix(QCoreApplication::translate("MainWindow", "  (m.s.n.m)", nullptr));
        label_18->setText(QCoreApplication::translate("MainWindow", "Descripci\303\263n del punto:", nullptr));
#if QT_CONFIG(tooltip)
        txtDesc_punto->setToolTip(QCoreApplication::translate("MainWindow", "Click para ver la descripcion completa", nullptr));
#endif // QT_CONFIG(tooltip)
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "Foto", nullptr));
        lblfoto->setText(QString());
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
