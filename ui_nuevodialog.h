/********************************************************************************
** Form generated from reading UI file 'nuevodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NUEVODIALOG_H
#define UI_NUEVODIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NuevoDialog
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QLabel *lblGrupo;
    QComboBox *cboLista;
    QListWidget *lwUnidades;
    QVBoxLayout *verticalLayout_2;
    QLabel *lblNombre;
    QLineEdit *txtNombre;
    QVBoxLayout *verticalLayout;
    QLabel *lblDescripcion;
    QPlainTextEdit *teDescripcion;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnGuardar;
    QPushButton *btnCancelar;

    void setupUi(QDialog *NuevoDialog)
    {
        if (NuevoDialog->objectName().isEmpty())
            NuevoDialog->setObjectName(QString::fromUtf8("NuevoDialog"));
        NuevoDialog->resize(383, 397);
        verticalLayout_3 = new QVBoxLayout(NuevoDialog);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lblGrupo = new QLabel(NuevoDialog);
        lblGrupo->setObjectName(QString::fromUtf8("lblGrupo"));

        horizontalLayout->addWidget(lblGrupo);

        cboLista = new QComboBox(NuevoDialog);
        cboLista->setObjectName(QString::fromUtf8("cboLista"));

        horizontalLayout->addWidget(cboLista);


        verticalLayout_3->addLayout(horizontalLayout);

        lwUnidades = new QListWidget(NuevoDialog);
        lwUnidades->setObjectName(QString::fromUtf8("lwUnidades"));

        verticalLayout_3->addWidget(lwUnidades);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lblNombre = new QLabel(NuevoDialog);
        lblNombre->setObjectName(QString::fromUtf8("lblNombre"));

        verticalLayout_2->addWidget(lblNombre);

        txtNombre = new QLineEdit(NuevoDialog);
        txtNombre->setObjectName(QString::fromUtf8("txtNombre"));

        verticalLayout_2->addWidget(txtNombre);


        verticalLayout_3->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lblDescripcion = new QLabel(NuevoDialog);
        lblDescripcion->setObjectName(QString::fromUtf8("lblDescripcion"));

        verticalLayout->addWidget(lblDescripcion);

        teDescripcion = new QPlainTextEdit(NuevoDialog);
        teDescripcion->setObjectName(QString::fromUtf8("teDescripcion"));

        verticalLayout->addWidget(teDescripcion);


        verticalLayout_3->addLayout(verticalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnGuardar = new QPushButton(NuevoDialog);
        btnGuardar->setObjectName(QString::fromUtf8("btnGuardar"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnGuardar->setIcon(icon);
        btnGuardar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnGuardar);

        btnCancelar = new QPushButton(NuevoDialog);
        btnCancelar->setObjectName(QString::fromUtf8("btnCancelar"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/img/cancel.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnCancelar->setIcon(icon1);
        btnCancelar->setIconSize(QSize(24, 24));

        horizontalLayout_2->addWidget(btnCancelar);


        verticalLayout_3->addLayout(horizontalLayout_2);


        retranslateUi(NuevoDialog);

        QMetaObject::connectSlotsByName(NuevoDialog);
    } // setupUi

    void retranslateUi(QDialog *NuevoDialog)
    {
        NuevoDialog->setWindowTitle(QCoreApplication::translate("NuevoDialog", "Dialog", nullptr));
        lblGrupo->setText(QCoreApplication::translate("NuevoDialog", "Grupo o compa\303\261\303\255a minera:", nullptr));
        lblNombre->setText(QCoreApplication::translate("NuevoDialog", "TextLabel", nullptr));
        lblDescripcion->setText(QCoreApplication::translate("NuevoDialog", "Descripci\303\263n:", nullptr));
#if QT_CONFIG(tooltip)
        btnGuardar->setToolTip(QCoreApplication::translate("NuevoDialog", "(Ctrl+S)", nullptr));
#endif // QT_CONFIG(tooltip)
        btnGuardar->setText(QCoreApplication::translate("NuevoDialog", "Guardar", nullptr));
#if QT_CONFIG(shortcut)
        btnGuardar->setShortcut(QCoreApplication::translate("NuevoDialog", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        btnCancelar->setToolTip(QCoreApplication::translate("NuevoDialog", "(Esc)", nullptr));
#endif // QT_CONFIG(tooltip)
        btnCancelar->setText(QCoreApplication::translate("NuevoDialog", "Cancelar", nullptr));
#if QT_CONFIG(shortcut)
        btnCancelar->setShortcut(QCoreApplication::translate("NuevoDialog", "Esc", nullptr));
#endif // QT_CONFIG(shortcut)
    } // retranslateUi

};

namespace Ui {
    class NuevoDialog: public Ui_NuevoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NUEVODIALOG_H
