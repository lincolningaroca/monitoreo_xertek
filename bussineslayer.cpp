#include "bussineslayer.h"
#include <QDebug>
#include <QDir>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QStandardPaths>

BussinesLayer::BussinesLayer()
  : m_db{QSqlDatabase::database("monitoreo")}, qry{m_db} {}

BussinesLayer::~BussinesLayer()
{

//  delete model;
//  qInfo()<<"Se elimino el puntero model"<<'\n';
}

//BussinesLayer::~BussinesLayer(){}

bool BussinesLayer::gpoMineroAction(const QVariantList &param, TypeStm modo) {

  //  SW::qry qry(db);

  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare(
            "INSERT INTO grupo_minero(nombre_grupo, descripcion) VALUES(?,?)");
      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }

      break;

    case TypeStm::UPDATE:
      qry.prepare(
            "UPDATE grupo_minero SET nombre_grupo=?,descripcion=? WHERE id=?");
      for (int i = 0; i < param.size(); ++i) {
          qry.addBindValue(param[i]);
          if (i == param.size())
            qry.addBindValue(param[i].toInt());
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;

    case TypeStm::DELETE:
      qry.prepare("DELETE FROM grupo_minero WHERE id=?");
      qry.addBindValue(param[0].toInt(), QSql::In);

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;
    }
  return true;
}

bool BussinesLayer::clienteMineroAction(const QVariantList &param, TypeStm modo) {
  //  QSqlQuery qry(db);
  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare("INSERT INTO cliente(nombre_unidad, descripcion, id_grupo) "
                  "VALUES(?,?,?)");
      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);
          //        if(i==param.size())
          //          qry.addBindValue(param[i].toInt());
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }

      break;

    case TypeStm::UPDATE:
      qry.prepare("UPDATE cliente SET nombre_unidad=?,descripcion=?, id_grupo=? "
                  "WHERE id=?");
      for (int i = 0; i < param.size(); ++i) {
          qry.addBindValue(param[i]);
          //        if(i==param.size())
          //          qry.addBindValue(param[i].toInt());
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;

    case TypeStm::DELETE:
      qry.prepare("DELETE FROM cliente WHERE id=?");
      qry.addBindValue(param[0].toInt(), QSql::In);

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;
    }
  return true;
}

bool BussinesLayer::monitoreoMineroAction(const QVariantList &param, TypeStm modo) {

  //  QSqlQuery qry(db);
  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare("INSERT INTO datos_monitoreo(codigo_estacion, "
                  "fecha_muestra,hora_muestra,"
                  " foto_1, foto_2, foto3, desc_punto, ph, tmp, od, ce, "
                  "coor_este, coor_norte, cota)"
                  " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      //      qry.next();
      m_lInsertId = qry.lastInsertId();
      break;

    case TypeStm::UPDATE:
      qry.prepare("UPDATE datos_monitoreo SET "
                  "codigo_estacion=?,fecha_muestra=?,hora_muestra=?,"
                  " foto_1=?,foto_2=?,foto3=?,desc_punto=?, ph=?,tmp=?,od=?,ce=?,"
                  " coor_este=?,coor_norte=?,cota=? WHERE id_estacion=?");
      for (int i = 0; i < param.size(); ++i) {
          qry.addBindValue(param[i]);
          //        if(i==param.size())
          //          qry.addBindValue(param[i].toInt());
        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;

    case TypeStm::DELETE:
      qry.prepare("DELETE FROM datos_monitoreo WHERE id_estacion=?");
      qry.addBindValue(param[0].toInt(), QSql::In);

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;
    }
  return true;
}

bool BussinesLayer::tablaMonitoreo(const QVariantList &param, TypeStm modo) {
  //  QSqlQuery qry(db);

  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare("INSERT INTO monitoreo(procedencia,id_cliente) VALUES(?,?)");
      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);

        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }

      break;

    case TypeStm::UPDATE:
      qry.prepare("UPDATE monitoreo SET procedencia=?,id_cliente=? WHERE id_monitoreo=?");

      for (int i = 0; i < param.size(); ++i) {
          qry.addBindValue(param[i]);

        }

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;

    case TypeStm::DELETE:
      qry.prepare("DELETE FROM monitoreo WHERE id_monitoreo=?");
      qry.addBindValue(param[0].toInt(), QSql::In);
      qry.addBindValue(param[1].toString(), QSql::In);

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;
    }
  return true;
}

bool BussinesLayer::tablaReferencia(const QVariantList &param, TypeStm modo) {
  //  QSqlQuery qry(db);

  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare("INSERT INTO referencias(referencia,id_monitoreo,matriz,norma_comparacion,"
                  " matriz_desc) VALUES(?,?,?,?,?)");
      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);

        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }

      break;

    case TypeStm::UPDATE:
      qry.prepare("UPDATE referencias SET referencia=?,id_monitoreo=?, matriz=?, "
                  "norma_comparacion=?, matriz_desc=? WHERE id_referencia=?");

      for (int i = 0; i < param.size(); ++i) {
          qry.addBindValue(param[i]);

        }

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;

    case TypeStm::DELETE:
      qry.prepare("DELETE FROM referencias WHERE id_referencias=?");
      qry.addBindValue(param[0].toInt(), QSql::In);
      qry.addBindValue(param[1].toString(), QSql::In);

      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
      break;
    }
  return true;
}

bool BussinesLayer::tablaReferenciaBatch(const QVariantList &ref,
                                         const QVariantList &id_monit,
                                         const QVariantList &mtrz,
                                         const QVariantList &norma,
                                         const QVariantList &mtrz_desc) {
  //  QSqlQuery qry(db);
  qry.prepare("INSERT INTO referencias(referencia,id_monitoreo,matriz,norma_comparacion,"
              " matriz_desc) VALUES(?,?,?,?,?)");
  qry.addBindValue(ref);
  qry.addBindValue(id_monit);
  qry.addBindValue(mtrz);
  qry.addBindValue(norma);
  qry.addBindValue(mtrz_desc);
  if (!qry.execBatch()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return false;
    }
  return true;
}

bool BussinesLayer::tablaRefMonitoreo(const QVariantList &param, TypeStm modo) {
  //  QSqlQuery qry(db);

  switch (modo) {
    case TypeStm::INSERT:
      qry.prepare("INSERT INTO referencias_monitoreo(id_referencia,id_estacion) VALUES(?,?)");
      for (int i = 0; i < param.size(); i++) {
          qry.addBindValue(param[i]);

        }
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return false;
        }
    case TypeStm::DELETE:
    case TypeStm::UPDATE:

      break;
    }
  return true;
}

bool BussinesLayer::updateReferencias(const QVariantList &param) {
  //  QSqlQuery qry(db);
  qry.prepare("UPDATE referencias SET matriz=?, norma_comparacion=? , "
              "matriz_desc=? WHERE id_referencia=?");

  for (int i = 0; i < param.size(); ++i) {
      qry.addBindValue(param[i]);

    }

  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return false;
    }
  return true;
}

bool BussinesLayer::codeExists(QStringView codEstacion, unsigned int idReferencia) {
  //  QSqlQuery qry(db);
  qry.prepare(
        "select count(*) from datos_monitoreo dm join referencias_monitoreo rm"
        " on dm.id_estacion=rm.id_estacion join referencias r on "
        "rm.id_referencia=r.id_referencia"
        " where dm.codigo_estacion=? and rm.id_referencia=?");

  qry.addBindValue(codEstacion.toString(), QSql::In);

  qry.addBindValue(idReferencia, QSql::In);

  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return false;
    }
  qry.next();
  //  if(qry.value(0).toInt()==1)
  //    return true;
  //  return false;
  return qry.value(0).toInt()==1;

}

bool BussinesLayer::RefExists(QStringView ref, unsigned int id_monit) {
  //  QSqlQuery qry(db);
  qry.prepare("SELECT COUNT(referencia) FROM referencias WHERE referencia=? AND id_monitoreo=?");
  qry.addBindValue(ref.toString());
  qry.addBindValue(id_monit);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return false;
    }
  qry.next();
  //  if(qry.value(0).toInt()==1)
  //    return true;
  //  return false;
  return qry.value(0).toInt()==1;

}

QVariantList BussinesLayer::selectData(const QVariant &id, Table info) {
  QVariantList dataList{};
  //  QSqlQuery qry(db);

  switch (info) {
    case Table::GPO_MINERO:
      qry.prepare("SELECT * FROM grupo_minero WHERE id=?");
      qry.addBindValue(id);
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          setError(qry);
          return dataList;
        }

      //      qInfo()<<qry.numRowsAffected();
      qry.next();
      dataList.append(qry.value(0));
      dataList.append(qry.value(1));
      dataList.append(qry.value(2));

      break;
    case Table::CLIENTE:
      qry.prepare("SELECT * FROM cliente WHERE id=?");
      qry.addBindValue(id);
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          setError(qry);
          return dataList;
        }
      qry.next();
      dataList.append(qry.value(0));
      dataList.append(qry.value(1));
      dataList.append(qry.value(2));
      dataList.append(qry.value(3));

      break;
    case Table::MONITOREO:
      qry.prepare("SELECT * FROM datos_monitoreo WHERE id=?");
      qry.addBindValue(id);
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          setError(qry);
          return dataList;
        }
      qry.next();
      dataList.append(qry.value(0));
      dataList.append(qry.value(1));
      dataList.append(qry.value(2));
      dataList.append(qry.value(3));
      dataList.append(qry.value(4));
      dataList.append(qry.value(5));
      dataList.append(qry.value(6));
      dataList.append(qry.value(7));

      break;
    case Table::CLIENTE_ALL:
      qry.prepare("SELECT id,nombre_unidad FROM cliente");
      qry.addBindValue(id);
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          setError(qry);
          return dataList;
        }
      while (qry.next()) {
          dataList.append(qry.value(0));
          dataList.append(qry.value(1));
          dataList.append(qry.value(2));
          dataList.append(qry.value(3));
          dataList.append(qry.value(4));
          dataList.append(qry.value(5));
        }
      break;
    }
  //  db.removeDb();
  return dataList;
}

QVariantList BussinesLayer::dataEstMonitoreo(unsigned int idReferencia, unsigned int idEstacion) {
  QVariantList dataList{};
  //  QSqlQuery qry(db);
  qry.prepare("SELECT codigo_estacion,fecha_muestra,hora_muestra,foto_1,foto_2,foto3,"
              "desc_punto,ph,tmp,od,ce,coor_este,coor_norte,cota,"
              //              "--tabla referencias"
              "referencia,matriz,norma_comparacion,"
              //              "--tabla referencias_monitoreo"
              "rm.id_referencia,rm.id_estacion"
              " FROM datos_monitoreo AS dm JOIN referencias_monitoreo AS rm"
              " ON dm.id_estacion=rm.id_estacion"
              " JOIN referencias AS r"
              " ON r.id_referencia=rm.id_referencia"
              " WHERE rm.id_referencia=? and rm.id_estacion=?");
  //  qry.addBindValue(idEstacion);
  qry.addBindValue(idReferencia);
  qry.addBindValue(idEstacion);

  //  qry.addBindValue(fecha);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  qry.next();
  dataList.append(qry.value(0));
  dataList.append(qry.value(1));
  dataList.append(qry.value(2));
  dataList.append(qry.value(3));
  dataList.append(qry.value(4));
  dataList.append(qry.value(5));
  dataList.append(qry.value(6));
  dataList.append(qry.value(7));
  dataList.append(qry.value(8));
  dataList.append(qry.value(9));
  dataList.append(qry.value(10));
  dataList.append(qry.value(11));
  dataList.append(qry.value(12));
  dataList.append(qry.value(13));
  dataList.append(qry.value(14));
  dataList.append(qry.value(15));
  dataList.append(qry.value(16));
  dataList.append(qry.value(17));
  dataList.append(qry.value(18));
  //  db.getConection().close();
  return dataList;
}

QStringList BussinesLayer::matrizList() noexcept {
  QStringList list{};
  //  QSqlQuery qry(db);
  qry.prepare("SELECT * FROM matriz_list()");

  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return list;
    }
  while (qry.next()) {
      list.append(qry.value(0).toString());
    }
  return list;
}

QStringList BussinesLayer::matrizDescList() noexcept {
  QStringList list{};
  //  QSqlQuery qry(db);
  qry.prepare("SELECT * FROM matriz_desc_list()");

  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return list;
    }
  while (qry.next()) {
      list.append(qry.value(0).toString());
    }
  return list;
}

QStringList BussinesLayer::nComparacionList() noexcept {
  QStringList list{};
  //  QSqlQuery qry(db);
  qry.prepare("SELECT * FROM nm_list()");

  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return list;
    }
  while (qry.next()) {
      list.append(qry.value(0).toString());
    }
  return list;
}

QHash<QString, QString> BussinesLayer::matriz() noexcept {
  QHash<QString, QString> data{};
  QStringList l1 = matrizList();
  QStringList l2 = matrizDescList();
  for (int i = 0; i < l1.size(); ++i) {
      data.insert(l1.value(i), l2.value(i));
    }
  return data;
}

QHash<unsigned int, QString>BussinesLayer::selectCodEstacion(unsigned int id_referencia) {
  QHash<unsigned int, QString> dataList{};

  qry.prepare("SELECT dm.id_estacion,dm.codigo_estacion FROM datos_monitoreo AS dm"
              " JOIN referencias_monitoreo AS rm ON dm.id_estacion=rm.id_estacion"
              " AND rm.id_referencia=?");
  qry.addBindValue(id_referencia);
  //  qry.addBindValue(fecha);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  while (qry.next()) {
      dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
    }
  return dataList;
}
QSqlQueryModel *BussinesLayer::selectCodEstacion_q(unsigned int idReferencia) noexcept {
  QSqlQueryModel* model{nullptr};
  //  QSqlQuery qry(db);
  qry.prepare("SELECT dm.id_estacion,dm.codigo_estacion,dm.fecha_muestra FROM "
              "datos_monitoreo AS dm JOIN referencias_monitoreo AS rm"
              " ON dm.id_estacion=rm.id_estacion AND rm.id_referencia=?");
  qry.addBindValue(idReferencia);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return model;
    }
  model = new QSqlQueryModel();
  model->setQuery(qry);
  qry.next();
  return model;
}

QHash<unsigned int, QString> BussinesLayer::selectDataClient() {
  QHash<unsigned int, QString> dataList{};

  //  QSqlQuery qry(db);
  qry.prepare("SELECT id,nombre_unidad FROM cliente");

  //  qry.addBindValue(fecha);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  while (qry.next()) {
      dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
    }
  return dataList;
}

QHash<unsigned int, QString> BussinesLayer::selectDataProc(unsigned int id) {
  QHash<unsigned int, QString> dataList{};

  //  QSqlQuery qry(db);
  qry.prepare("SELECT id_monitoreo,procedencia FROM monitoreo WHERE id_cliente=?");

  qry.addBindValue(id);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  while (qry.next()) {
      dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
    }
  return dataList;
}

QVariantList BussinesLayer::selectDataRef_All(unsigned int idRef) {
  //  QSqlQuery qry(db);
  QVariantList data{};
  qry.prepare("SELECT matriz,norma_comparacion,matriz_desc FROM referencias "
              "WHERE id_referencia=?");

  qry.addBindValue(idRef);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return data;
    }
  qry.next();
  data.append(qry.value(0));
  data.append(qry.value(1));
  data.append(qry.value(2));
  data.append(qry.value(3));

  return data;
}

QHash<unsigned int, QString> BussinesLayer::selectDataRef(unsigned int id) {
  QHash<unsigned int, QString> dataList{};

  //  QSqlQuery qry(db);
  qry.prepare("SELECT id_referencia,referencia FROM referencias WHERE id_monitoreo=?");

  qry.addBindValue(id);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  while (qry.next()) {
      dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
    }
  return dataList;
}

QHash<unsigned int, QString> BussinesLayer::selectDataMonitoreo(unsigned int id) {
  QHash<unsigned int, QString> dataList{};

  //  QSqlQuery qry(db);
  qry.prepare("SELECT id_monitoreo,procedencia FROM monitoreo WHERE id_cliente=?");

  qry.addBindValue(id);
  if (!qry.exec()) {
      //      m_errorMessage = qry.lastError().text();
      //      m_errorCode = qry.lastError().nativeErrorCode();
      setError(qry);
      return dataList;
    }
  while (qry.next()) {
      dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
    }
  return dataList;
}

QHash<unsigned int, QString> BussinesLayer::gpoMineroList(Table t, unsigned int id) {

  QHash<unsigned int, QString> dataList{};

  switch (t) {
    case Table::GPO_MINERO:

      qry.prepare("SELECT id, nombre_grupo FROM grupo_minero");
      if (!qry.exec()) {
          //          m_errorMessage = qry.lastError().text();
          //          m_errorCode = qry.lastError().nativeErrorCode();
          setError(qry);
          return dataList;
        }
      while (qry.next()) {
          dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
        }
      //      return dataList;
      break;

    case Table::CLIENTE:
      //      QSqlQuery qry;
      qry.prepare("SELECT id, nombre_unidad FROM cliente WHERE id_grupo=?");
      qry.addBindValue(id);
      if (!qry.exec()) {
          setError(qry);
          return dataList;
        }
      while (qry.next()) {
          dataList.insert(qry.value(0).toUInt(), qry.value(1).toString());
        }
      //      return dataList;
      break;

    default:
      //      return dataList;
      break;
    }

  return dataList;
}

bool BussinesLayer::createDirPictures() const noexcept{
  QDir dir;
  dir.setPath(relativePath());
  if (dir.exists()) return false;
  return dir.mkpath(relativePath());
}
QString BussinesLayer::relativePath() const noexcept{
  //  QString& relativePath;
  QStringList list = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);
  QString& relativePath = list.first();
  relativePath.append("/");
  relativePath.append("pictures_folder");
  return relativePath;
}

void BussinesLayer::setError(const SW::qry &qry) noexcept
{
  m_errorMessage.clear();
  m_errorCode.clear();
  m_errorMessage = qry.lastError().text();
  m_errorCode = qry.lastError().nativeErrorCode();
}
