#include "referenciadialog.h"
#include "ui_referenciadialog.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QDebug>
#include "nuevovarios.h"

ReferenciaDialog::ReferenciaDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::ReferenciaDialog)
{
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog|Qt::MSWindowsFixedSizeDialogHint);
  loadDataClient();
  loadDataProcedencia();
  fillCboMatriz();
  fillCboNComparacion();
  ui->lblMatriz->setStyleSheet("color:#007af4;");
}

ReferenciaDialog::~ReferenciaDialog()
{
  delete ui;
}

void ReferenciaDialog::loadDataClient() noexcept
{
  m_dataClient=m_bLayer.selectDataClient();
  ui->cboCliente->addItems(m_dataClient.values());
}

void ReferenciaDialog::loadDataProcedencia() noexcept
{
  m_dataProcedencia=m_bLayer.selectDataProc(m_dataClient.key(ui->cboCliente->currentText()));
  ui->cboProcedencia->addItems(m_dataProcedencia.values());
}

void ReferenciaDialog::fillCboMatriz() noexcept
{

  ui->cboMatriz->clear();
  m_matrizList=m_bLayer.matriz();
  ui->cboMatriz->addItems(m_matrizList.keys());
  ui->lblMatriz->setText(m_matrizList.value(ui->cboMatriz->currentText()));
}

void ReferenciaDialog::fillCboNComparacion() noexcept
{
  ui->cboNorma->clear();
  ui->cboNorma->addItems(m_bLayer.nComparacionList());
}

void ReferenciaDialog::on_btnAceptar_clicked()
{
  if(ui->txtReferencia->toPlainText().simplified().isEmpty()){
      QMessageBox::warning(this,qApp->applicationName(),"La referencia es requerida.");
      ui->txtReferencia->setFocus();
      return;
    }
  if(m_bLayer.RefExists(ui->txtReferencia->toPlainText(),m_dataProcedencia.key(ui->cboProcedencia->currentText()))){
      QMessageBox::warning(this,qApp->applicationName(),"Ya existe una referencia con este nombre:\n"+
                           ui->txtReferencia->toPlainText().toUpper());
      ui->txtReferencia->selectAll();
      ui->txtReferencia->setFocus(Qt::OtherFocusReason);
      return;
    }

  QVariantList param{};
  param.append(ui->txtReferencia->toPlainText().toUpper());
  param.append(m_dataProcedencia.key(ui->cboProcedencia->currentText()));
  param.append(ui->cboMatriz->currentText());
  param.append(ui->cboNorma->currentText());
  param.append(m_matrizList.value(ui->cboMatriz->currentText()));
  if(!m_bLayer.tablaReferencia(param,BussinesLayer::TypeStm::INSERT)){
      QMessageBox::critical(this,qApp->applicationName(),"Error al guadar los datos."+
                            m_bLayer.errorMessage());
      return;
    }
  QMessageBox::information(this,qApp->applicationName(),"Datos guardados.");
  accept();
}


void ReferenciaDialog::on_cboCliente_activated(int index)
{
  Q_UNUSED(index)
  ui->cboProcedencia->clear();
  loadDataProcedencia();
}


void ReferenciaDialog::on_btnCancelar_clicked()
{
  reject();
}


void ReferenciaDialog::on_cboMatriz_activated(const QString &arg1)
{
  ui->lblMatriz->setText(m_matrizList.value(arg1));
}


void ReferenciaDialog::on_btnNuevaProcedencia_clicked()
{
  NuevoVarios nVarios{this};
  nVarios.setWindowTitle(qApp->applicationName().append(" - Nueva procedencia"));
  if(nVarios.exec()==QDialog::Accepted)
    loadDataProcedencia();
}



