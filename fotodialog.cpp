#include "fotodialog.h"
#include "ui_fotodialog.h"

FotoDialog::FotoDialog(QStringView title, QImage foto, QWidget *parent)
  : QDialog(parent), ui(new Ui::FotoDialog), m_tittle{title}, m_foto{foto} {
  ui->setupUi(this);
  setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  setWindowTitle(m_tittle.toString());
  //  _foto=foto;
  loadFoto();
}

FotoDialog::~FotoDialog() { delete ui;}

void FotoDialog::loadFoto() {
  ui->lblFoto->setPixmap(QPixmap::fromImage(m_foto).scaled(this->size(),Qt::KeepAspectRatio));
  this->resize(ui->lblFoto->size());
}
