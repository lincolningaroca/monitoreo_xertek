#include "frmprincipal.h"
#include <QApplication>
#include <QStandardPaths>
#include <QDebug>
#include <QMessageBox>
#include "dbconection.h"
#include "bussineslayer.h"


int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  a.setStyle("Fusion");
  a.setApplicationName("SWDatos_monitoreo");
  a.setApplicationVersion("1.0");
  a.setOrganizationName("SWSystem's");
  a.setOrganizationDomain("swsystems.com");

  //conectarse a la base de datos.
  DbConection db;
  if(!db.connect("monitoreo")){
    QMessageBox::critical(nullptr,qApp->applicationName(),"Error al conectarse a la base de datos.\n"+
                                                              db.errorMessage());
    return 1;
  }

  //creacion de la carpeta, por defecto donde se guardaran las imagenes del sistema.
  BussinesLayer b;
  if(!b.createDirPictures())
    qInfo()<<"La carpeta ya existe";
  else
    qInfo()<<"La carpeta fue creada";

  FrmPrincipal w;
  w.setWindowTitle(a.applicationName());
  //cargar configuraciones de la aplicación
  //  w.readSettings();
  w.show();
  return a.exec();

}
