/********************************************************************************
** Form generated from reading UI file 'Desc_pdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DESC_PDIALOG_H
#define UI_DESC_PDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Desc_pDialog
{
public:
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *txtDescPunto;

    void setupUi(QDialog *Desc_pDialog)
    {
        if (Desc_pDialog->objectName().isEmpty())
            Desc_pDialog->setObjectName(QString::fromUtf8("Desc_pDialog"));
        Desc_pDialog->resize(342, 204);
        verticalLayout = new QVBoxLayout(Desc_pDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        txtDescPunto = new QPlainTextEdit(Desc_pDialog);
        txtDescPunto->setObjectName(QString::fromUtf8("txtDescPunto"));
        txtDescPunto->setReadOnly(true);

        verticalLayout->addWidget(txtDescPunto);


        retranslateUi(Desc_pDialog);

        QMetaObject::connectSlotsByName(Desc_pDialog);
    } // setupUi

    void retranslateUi(QDialog *Desc_pDialog)
    {
        Desc_pDialog->setWindowTitle(QCoreApplication::translate("Desc_pDialog", "Descripci\303\263n del punto", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Desc_pDialog: public Ui_Desc_pDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DESC_PDIALOG_H
