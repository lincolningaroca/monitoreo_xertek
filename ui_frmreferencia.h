/********************************************************************************
** Form generated from reading UI file 'frmreferencia.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRMREFERENCIA_H
#define UI_FRMREFERENCIA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FrmReferencia
{
public:
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QComboBox *cboCliente;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QComboBox *cboProcedencia;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QTextEdit *txtReferencia;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *cboMatriz;
    QToolButton *btnMatriz;
    QLabel *label_2;
    QComboBox *cboNorma;
    QToolButton *btnNOrma;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnAceptar;
    QPushButton *btnCancelar;

    void setupUi(QDialog *FrmReferencia)
    {
        if (FrmReferencia->objectName().isEmpty())
            FrmReferencia->setObjectName(QString::fromUtf8("FrmReferencia"));
        FrmReferencia->resize(533, 351);
        verticalLayout_4 = new QVBoxLayout(FrmReferencia);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(FrmReferencia);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        cboCliente = new QComboBox(groupBox);
        cboCliente->setObjectName(QString::fromUtf8("cboCliente"));

        verticalLayout->addWidget(cboCliente);


        verticalLayout_4->addWidget(groupBox);

        groupBox_3 = new QGroupBox(FrmReferencia);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        cboProcedencia = new QComboBox(groupBox_3);
        cboProcedencia->setObjectName(QString::fromUtf8("cboProcedencia"));

        verticalLayout_3->addWidget(cboProcedencia);


        verticalLayout_4->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(FrmReferencia);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        txtReferencia = new QTextEdit(groupBox_2);
        txtReferencia->setObjectName(QString::fromUtf8("txtReferencia"));

        verticalLayout_2->addWidget(txtReferencia);


        verticalLayout_4->addWidget(groupBox_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(FrmReferencia);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        cboMatriz = new QComboBox(FrmReferencia);
        cboMatriz->setObjectName(QString::fromUtf8("cboMatriz"));

        gridLayout->addWidget(cboMatriz, 0, 1, 1, 1);

        btnMatriz = new QToolButton(FrmReferencia);
        btnMatriz->setObjectName(QString::fromUtf8("btnMatriz"));

        gridLayout->addWidget(btnMatriz, 0, 2, 1, 1);

        label_2 = new QLabel(FrmReferencia);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        cboNorma = new QComboBox(FrmReferencia);
        cboNorma->setObjectName(QString::fromUtf8("cboNorma"));

        gridLayout->addWidget(cboNorma, 1, 1, 1, 1);

        btnNOrma = new QToolButton(FrmReferencia);
        btnNOrma->setObjectName(QString::fromUtf8("btnNOrma"));

        gridLayout->addWidget(btnNOrma, 1, 2, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnAceptar = new QPushButton(FrmReferencia);
        btnAceptar->setObjectName(QString::fromUtf8("btnAceptar"));

        horizontalLayout->addWidget(btnAceptar);

        btnCancelar = new QPushButton(FrmReferencia);
        btnCancelar->setObjectName(QString::fromUtf8("btnCancelar"));

        horizontalLayout->addWidget(btnCancelar);


        verticalLayout_4->addLayout(horizontalLayout);


        retranslateUi(FrmReferencia);

        QMetaObject::connectSlotsByName(FrmReferencia);
    } // setupUi

    void retranslateUi(QDialog *FrmReferencia)
    {
        FrmReferencia->setWindowTitle(QCoreApplication::translate("FrmReferencia", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("FrmReferencia", "Cliente / unidad minera", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("FrmReferencia", "Procedencia:", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("FrmReferencia", "Referencia:", nullptr));
        label->setText(QCoreApplication::translate("FrmReferencia", "Matriz:", nullptr));
#if QT_CONFIG(tooltip)
        btnMatriz->setToolTip(QCoreApplication::translate("FrmReferencia", "Agregar matriz", nullptr));
#endif // QT_CONFIG(tooltip)
        btnMatriz->setText(QCoreApplication::translate("FrmReferencia", "...", nullptr));
        label_2->setText(QCoreApplication::translate("FrmReferencia", "Norma comparaci\303\263n:", nullptr));
#if QT_CONFIG(tooltip)
        btnNOrma->setToolTip(QCoreApplication::translate("FrmReferencia", "Agregar norma", nullptr));
#endif // QT_CONFIG(tooltip)
        btnNOrma->setText(QCoreApplication::translate("FrmReferencia", "...", nullptr));
        btnAceptar->setText(QCoreApplication::translate("FrmReferencia", "Aceptar", nullptr));
        btnCancelar->setText(QCoreApplication::translate("FrmReferencia", "Cancelar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FrmReferencia: public Ui_FrmReferencia {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMREFERENCIA_H
