/********************************************************************************
** Form generated from reading UI file 'fotodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FOTODIALOG_H
#define UI_FOTODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FotoDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lblFoto;

    void setupUi(QDialog *FotoDialog)
    {
        if (FotoDialog->objectName().isEmpty())
            FotoDialog->setObjectName(QString::fromUtf8("FotoDialog"));
        FotoDialog->resize(717, 547);
        verticalLayout = new QVBoxLayout(FotoDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lblFoto = new QLabel(FotoDialog);
        lblFoto->setObjectName(QString::fromUtf8("lblFoto"));
        lblFoto->setScaledContents(false);

        verticalLayout->addWidget(lblFoto);


        retranslateUi(FotoDialog);

        QMetaObject::connectSlotsByName(FotoDialog);
    } // setupUi

    void retranslateUi(QDialog *FotoDialog)
    {
        FotoDialog->setWindowTitle(QCoreApplication::translate("FotoDialog", "Dialog", nullptr));
        lblFoto->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FotoDialog: public Ui_FotoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FOTODIALOG_H
