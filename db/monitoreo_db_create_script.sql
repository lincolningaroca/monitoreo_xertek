--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

-- Started on 2021-10-15 20:58:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 639 (class 1247 OID 16396)
-- Name: mtrz; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.mtrz AS ENUM (
    'AN1',
    'AN2',
    'AR3',
    'AR4',
    'AR5',
    'AC6',
    'AC7',
    'AC8',
    'AS9',
    'AS10',
    'AS11',
    'AS12',
    'AP13',
    'AP14',
    'AP15',
    'AP16',
    'AP17',
    'AP18',
    'AIRE',
    'RUIDO AMBIENTAL',
    'EMISIONES',
    'SUELO'
);


ALTER TYPE public.mtrz OWNER TO postgres;

--
-- TOC entry 642 (class 1247 OID 16434)
-- Name: mtrz_desc; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.mtrz_desc AS ENUM (
    'AGUA NATURAL (SUBTERRANEA)',
    'AGUA NATURAL (SUPERFICIAL)',
    'AGUA RESIDUAL (DOMESTICA)',
    'AGUA RESIDUAL (INDUSTRIAL)',
    'AGUA RESIDUAL (MUNICIPAL)',
    'AGUA DE BEBIDA',
    'AGUA DE PISCINA',
    'AGUA DE LAGUNA ARTIFICIAL',
    'MAR',
    'SALOBRE',
    'SALMUERA',
    'AGUA DE INYECCION',
    'AGUA DE CIRCULACION',
    'AGUA DE ALIMENTACION',
    'AGUA DE CALDERAS',
    'AGUAS DE LIXIVIACION',
    'AGUA PURIFICADA',
    'AP18-AGUA DE INYECCION',
    'AIRE',
    'RUIDO AMBIENTAL',
    'EMISIONES',
    'SUELO'
);


ALTER TYPE public.mtrz_desc OWNER TO postgres;

--
-- TOC entry 672 (class 1247 OID 16606)
-- Name: n_comparacion; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.n_comparacion AS ENUM (
    'D.S.N°004-2017-MINAM. Categoría 3.',
    'D.S. 010-2010-MINAM',
    'DS.031-2010-SA calidad de agua de consumo humano',
    'D.S.N°004-2017-MINAM. Categoría 4',
    'D.S. N° 003-2017 MINAM',
    'D.S.N°004-2017-MINAM. Categoría 3 / D.S.N°010-2010-MINAM',
    'D.S.N°004-2017-MINAM'
);


ALTER TYPE public.n_comparacion OWNER TO postgres;

--
-- TOC entry 211 (class 1255 OID 16471)
-- Name: datos_monit(character, date, time without time zone, text, bytea, bytea, bytea, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.datos_monit(cod_estacion character, fecha date, hora time without time zone, des text, f1 bytea, f2 bytea, f3 bytea, id_cli integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
									   DECLARE
									   codigo text;
									   BEGIN
									   INSERT INTO datos_monitoreo(codigo_estacion, fecha_muestra,hora_muestra,
                   						descripcion, foto_1, foto_2, foto3, id_cliente)
                  						VALUES(cod_estacion,fecha,hora,des,f1,f2,f3,id_cli)
										RETURNING codigo_estacion INTO codigo;
										
										RETURN codigo;
										END;
										$$;


ALTER FUNCTION public.datos_monit(cod_estacion character, fecha date, hora time without time zone, des text, f1 bytea, f2 bytea, f3 bytea, id_cli integer) OWNER TO postgres;

--
-- TOC entry 212 (class 1255 OID 16472)
-- Name: listar(character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.listar(mes character) RETURNS TABLE(id integer, cod character)
    LANGUAGE sql
    AS $$
SELECT id_estacion,codigo_estacion FROM datos_monitoreo 
WHERE fecha_muestra::text SIMILAR TO '%-'||mes||'-%';
$$;


ALTER FUNCTION public.listar(mes character) OWNER TO postgres;

--
-- TOC entry 213 (class 1255 OID 16473)
-- Name: listar_por_fecha(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.listar_por_fecha(integer, integer) RETURNS TABLE(id integer, cod character)
    LANGUAGE sql
    AS $_$
select id_estacion,codigo_estacion from datos_monitoreo where extract(year from fecha_muestra)=$1 and
extract(month from fecha_muestra)=$2
$_$;


ALTER FUNCTION public.listar_por_fecha(integer, integer) OWNER TO postgres;

--
-- TOC entry 214 (class 1255 OID 16474)
-- Name: matriz_desc_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.matriz_desc_list() RETURNS SETOF public.mtrz_desc
    LANGUAGE sql
    AS $$
select unnest(enum_range(NULL::mtrz_desc))
$$;


ALTER FUNCTION public.matriz_desc_list() OWNER TO postgres;

--
-- TOC entry 215 (class 1255 OID 16475)
-- Name: matriz_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.matriz_list() RETURNS SETOF public.mtrz
    LANGUAGE sql
    AS $$
select unnest(enum_range(NULL::mtrz))
$$;


ALTER FUNCTION public.matriz_list() OWNER TO postgres;

--
-- TOC entry 216 (class 1255 OID 16629)
-- Name: nm_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.nm_list() RETURNS SETOF public.n_comparacion
    LANGUAGE sql
    AS $$
select unnest(enum_range(NULL::n_comparacion));
$$;


ALTER FUNCTION public.nm_list() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 16476)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nombre_unidad character varying(255) NOT NULL,
    descripcion text,
    id_grupo integer NOT NULL
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16482)
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO postgres;

--
-- TOC entry 3061 (class 0 OID 0)
-- Dependencies: 201
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;


--
-- TOC entry 202 (class 1259 OID 16484)
-- Name: datos_monitoreo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.datos_monitoreo (
    id_estacion integer NOT NULL,
    codigo_estacion character varying(50) NOT NULL,
    fecha_muestra date NOT NULL,
    hora_muestra time without time zone NOT NULL,
    foto_1 text NOT NULL,
    foto_2 text,
    foto3 text,
    desc_punto text,
    ph numeric(8,2),
    tmp numeric(8,2),
    od numeric(8,2),
    ce numeric(8,2),
    coor_este numeric(10,2),
    coor_norte numeric(10,2),
    cota numeric(10,2)
);


ALTER TABLE public.datos_monitoreo OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16490)
-- Name: datos_monitoreo_id_estacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.datos_monitoreo_id_estacion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.datos_monitoreo_id_estacion_seq OWNER TO postgres;

--
-- TOC entry 3062 (class 0 OID 0)
-- Dependencies: 203
-- Name: datos_monitoreo_id_estacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.datos_monitoreo_id_estacion_seq OWNED BY public.datos_monitoreo.id_estacion;


--
-- TOC entry 204 (class 1259 OID 16492)
-- Name: grupo_minero; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grupo_minero (
    id integer NOT NULL,
    nombre_grupo character varying(255) NOT NULL,
    descripcion text
);


ALTER TABLE public.grupo_minero OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16498)
-- Name: grupo_minero_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grupo_minero_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grupo_minero_id_seq OWNER TO postgres;

--
-- TOC entry 3063 (class 0 OID 0)
-- Dependencies: 205
-- Name: grupo_minero_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grupo_minero_id_seq OWNED BY public.grupo_minero.id;


--
-- TOC entry 206 (class 1259 OID 16500)
-- Name: monitoreo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.monitoreo (
    id_monitoreo integer NOT NULL,
    procedencia text NOT NULL,
    id_cliente integer NOT NULL
);


ALTER TABLE public.monitoreo OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16506)
-- Name: monitoreo_id_monitoreo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.monitoreo_id_monitoreo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.monitoreo_id_monitoreo_seq OWNER TO postgres;

--
-- TOC entry 3064 (class 0 OID 0)
-- Dependencies: 207
-- Name: monitoreo_id_monitoreo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.monitoreo_id_monitoreo_seq OWNED BY public.monitoreo.id_monitoreo;


--
-- TOC entry 208 (class 1259 OID 16508)
-- Name: referencias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.referencias (
    id_referencia integer NOT NULL,
    referencia text NOT NULL,
    id_monitoreo integer NOT NULL,
    matriz public.mtrz,
    norma_comparacion public.n_comparacion,
    matriz_desc public.mtrz_desc
);


ALTER TABLE public.referencias OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16514)
-- Name: referencias_id_referencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.referencias_id_referencia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referencias_id_referencia_seq OWNER TO postgres;

--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 209
-- Name: referencias_id_referencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.referencias_id_referencia_seq OWNED BY public.referencias.id_referencia;


--
-- TOC entry 210 (class 1259 OID 16516)
-- Name: referencias_monitoreo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.referencias_monitoreo (
    id_referencia integer NOT NULL,
    id_estacion integer NOT NULL
);


ALTER TABLE public.referencias_monitoreo OWNER TO postgres;

--
-- TOC entry 2898 (class 2604 OID 16519)
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.cliente_id_seq'::regclass);


--
-- TOC entry 2899 (class 2604 OID 16520)
-- Name: datos_monitoreo id_estacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.datos_monitoreo ALTER COLUMN id_estacion SET DEFAULT nextval('public.datos_monitoreo_id_estacion_seq'::regclass);


--
-- TOC entry 2900 (class 2604 OID 16521)
-- Name: grupo_minero id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_minero ALTER COLUMN id SET DEFAULT nextval('public.grupo_minero_id_seq'::regclass);


--
-- TOC entry 2901 (class 2604 OID 16522)
-- Name: monitoreo id_monitoreo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monitoreo ALTER COLUMN id_monitoreo SET DEFAULT nextval('public.monitoreo_id_monitoreo_seq'::regclass);


--
-- TOC entry 2902 (class 2604 OID 16523)
-- Name: referencias id_referencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias ALTER COLUMN id_referencia SET DEFAULT nextval('public.referencias_id_referencia_seq'::regclass);


--
-- TOC entry 2904 (class 2606 OID 16525)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2908 (class 2606 OID 16527)
-- Name: datos_monitoreo datos_monitoreo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.datos_monitoreo
    ADD CONSTRAINT datos_monitoreo_pkey PRIMARY KEY (id_estacion);


--
-- TOC entry 2910 (class 2606 OID 16529)
-- Name: grupo_minero grupo_minero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_minero
    ADD CONSTRAINT grupo_minero_pkey PRIMARY KEY (id);


--
-- TOC entry 2914 (class 2606 OID 16531)
-- Name: monitoreo monitoreo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monitoreo
    ADD CONSTRAINT monitoreo_pkey PRIMARY KEY (id_monitoreo);


--
-- TOC entry 2916 (class 2606 OID 16533)
-- Name: monitoreo proc_constraint; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monitoreo
    ADD CONSTRAINT proc_constraint UNIQUE (procedencia);


--
-- TOC entry 2920 (class 2606 OID 16535)
-- Name: referencias_monitoreo referencias_monitoreo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias_monitoreo
    ADD CONSTRAINT referencias_monitoreo_pkey PRIMARY KEY (id_referencia, id_estacion);


--
-- TOC entry 2918 (class 2606 OID 16537)
-- Name: referencias referencias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias
    ADD CONSTRAINT referencias_pkey PRIMARY KEY (id_referencia);


--
-- TOC entry 2912 (class 2606 OID 16539)
-- Name: grupo_minero unique_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_minero
    ADD CONSTRAINT unique_name UNIQUE (nombre_grupo);


--
-- TOC entry 2906 (class 2606 OID 16541)
-- Name: cliente unique_nombre_unidad; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT unique_nombre_unidad UNIQUE (nombre_unidad);


--
-- TOC entry 2921 (class 2606 OID 16542)
-- Name: cliente grupo_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT grupo_cliente FOREIGN KEY (id_grupo) REFERENCES public.grupo_minero(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2922 (class 2606 OID 16547)
-- Name: monitoreo monitoreo_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monitoreo
    ADD CONSTRAINT monitoreo_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES public.cliente(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2923 (class 2606 OID 16552)
-- Name: referencias referencias_id_monitoreo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias
    ADD CONSTRAINT referencias_id_monitoreo_fkey FOREIGN KEY (id_monitoreo) REFERENCES public.monitoreo(id_monitoreo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2924 (class 2606 OID 16557)
-- Name: referencias_monitoreo referencias_monitoreo_id_estacion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias_monitoreo
    ADD CONSTRAINT referencias_monitoreo_id_estacion_fkey FOREIGN KEY (id_estacion) REFERENCES public.datos_monitoreo(id_estacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2925 (class 2606 OID 16562)
-- Name: referencias_monitoreo referencias_monitoreo_id_referencia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.referencias_monitoreo
    ADD CONSTRAINT referencias_monitoreo_id_referencia_fkey FOREIGN KEY (id_referencia) REFERENCES public.referencias(id_referencia) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2021-10-15 20:58:57

--
-- PostgreSQL database dump complete
--

