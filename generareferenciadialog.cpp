#include "generareferenciadialog.h"
#include "ui_generareferenciadialog.h"
#include <QDebug>
#include <QMessageBox>

GeneraReferenciaDialog::GeneraReferenciaDialog(unsigned int id_cliente,unsigned int idMonit,QWidget *parent)
  : QDialog(parent), ui(new Ui::GeneraReferenciaDialog), m_idCliente(id_cliente), m_idMonit(idMonit){
  ui->setupUi(this);
  loadProcedencia();
  loadReferencias();
  checkRefExists();
}

GeneraReferenciaDialog::~GeneraReferenciaDialog() { delete ui; }

void GeneraReferenciaDialog::loadProcedencia() {
  m_dataProcedencia = m_bLayer.selectDataProc(m_idCliente);
  ui->cboProcedencia->addItems(m_dataProcedencia.values());

  QObject::connect(ui->cboProcedencia, QOverload<int>::of(&QComboBox::activated), this,
                   &GeneraReferenciaDialog::loadReferencias);
}

void GeneraReferenciaDialog::loadReferencias() {
  ui->lwReferencia->clear();
  m_dataReferencia = m_bLayer.selectDataRef(m_dataProcedencia.key(ui->cboProcedencia->currentText()));
  ui->lwReferencia->addItems(m_dataReferencia.values());

//  QListWidgetItem *chkBoxItem{nullptr};
  for (int i = 0; i < ui->lwReferencia->count(); ++i) {
      QListWidgetItem *chkBoxItem = ui->lwReferencia->item(i);
      chkBoxItem->setFlags(chkBoxItem->flags() | Qt::ItemIsUserCheckable);
      chkBoxItem->setCheckState(Qt::Checked);
    }
  checkRefExists();
}

void GeneraReferenciaDialog::on_btnCancelar_clicked() { reject(); }

void GeneraReferenciaDialog::on_btnGenerar_clicked() {
  QVariantList ref{};
  QVariantList id_monit{};
  QVariantList mtrz{};
  QVariantList norma{};
  QVariantList mtrz_desc{};
  for (int i = 0; i < ui->lwReferencia->count(); ++i) {

      if (ui->lwReferencia->item(i)->checkState() == Qt::Checked) {

          ref.append(ui->lwReferencia->item(i)->data(Qt::DisplayRole));
          id_monit.append(m_idMonit);
          QVariantList dat = m_bLayer.selectDataRef_All(m_dataReferencia.key(
                                                        ui->lwReferencia->item(i)->data(Qt::DisplayRole).toString()));
          mtrz.append(dat.value(0));
          norma.append(dat.value(1));
          mtrz_desc.append(dat.value(2));
        }
      //      qInfo()<<ui->lwReferencia->item(i)->data(Qt::DisplayRole).toString().simplified();
    }
  if (ref.isEmpty())
    return;
  if (!m_bLayer.tablaReferenciaBatch(ref, id_monit, mtrz, norma, mtrz_desc)) {
      QMessageBox::critical(this, qApp->applicationName(), "Error.\n" + m_bLayer.errorMessage());
      return;
    }
  QMessageBox::information(this, qApp->applicationName(), "Datos generados.");
  accept();
  //  qInfo()<<m_bLayer.selectDataRef_All(m_dataReferencia.key(ui->lwReferencia->currentIndex().data().toString()));
}

void GeneraReferenciaDialog::checkRefExists() {
  for (int i = 0; i < ui->lwReferencia->count(); ++i) {
      if (m_bLayer.RefExists(ui->lwReferencia->item(i)->data(Qt::DisplayRole).toString(), m_idMonit)) {
          //        QMessageBox::warning(this,qApp->applicationName(),"No se puede
          //        crear este código.");
          ui->lwReferencia->item(i)->setCheckState(Qt::Unchecked);
          ui->lwReferencia->item(i)->setFlags(Qt::ItemFlags() ^ Qt::ItemIsEnabled);
        }
    }
}
