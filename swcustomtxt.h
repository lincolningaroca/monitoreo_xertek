#pragma once

#include <QLineEdit>
#include <QMouseEvent>
#include <QWidget>

struct SWCustomTxt : public QLineEdit
{
  Q_OBJECT
public:
  explicit SWCustomTxt(QWidget *parent=nullptr);
signals:
  void clicked();

  // QPaintDevice interface
public:
  virtual QPaintEngine *paintEngine() const override;

  // QWidget interface
protected:
  virtual void mousePressEvent(QMouseEvent *event) override;
};


