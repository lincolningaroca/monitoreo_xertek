#pragma once

#include <QDialog>

namespace Ui {
class Desc_pDialog;
}

class Desc_pDialog : public QDialog {
  Q_OBJECT

public:
  explicit Desc_pDialog(unsigned int mode, QString descr, QWidget *parent = nullptr);
  explicit Desc_pDialog(QString descr, QWidget *parent = nullptr);
  //  Desc_pDialog(QWidget *parent = nullptr);
  ~Desc_pDialog();

  inline QString desc() const { return m_desc; }
//  void setDesc(const QString &d) { m_d = d; }

private:
  Ui::Desc_pDialog *ui;
  QPushButton *m_btnGuardar{nullptr};
  QPushButton *m_btnCancelar{nullptr};
  QString m_desc{""};
  const QString m_d{""};
  const unsigned int m_mo{};
};
