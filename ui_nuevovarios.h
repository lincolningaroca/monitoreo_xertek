/********************************************************************************
** Form generated from reading UI file 'nuevovarios.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NUEVOVARIOS_H
#define UI_NUEVOVARIOS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NuevoVarios
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QComboBox *cboCliente;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QTextEdit *txtProcedencia;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnAceptar;
    QPushButton *btnCancelar;

    void setupUi(QDialog *NuevoVarios)
    {
        if (NuevoVarios->objectName().isEmpty())
            NuevoVarios->setObjectName(QString::fromUtf8("NuevoVarios"));
        NuevoVarios->resize(473, 240);
        verticalLayout_3 = new QVBoxLayout(NuevoVarios);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(NuevoVarios);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        cboCliente = new QComboBox(groupBox);
        cboCliente->setObjectName(QString::fromUtf8("cboCliente"));

        verticalLayout->addWidget(cboCliente);


        verticalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(NuevoVarios);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        txtProcedencia = new QTextEdit(groupBox_2);
        txtProcedencia->setObjectName(QString::fromUtf8("txtProcedencia"));

        verticalLayout_2->addWidget(txtProcedencia);


        verticalLayout_3->addWidget(groupBox_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnAceptar = new QPushButton(NuevoVarios);
        btnAceptar->setObjectName(QString::fromUtf8("btnAceptar"));

        horizontalLayout->addWidget(btnAceptar);

        btnCancelar = new QPushButton(NuevoVarios);
        btnCancelar->setObjectName(QString::fromUtf8("btnCancelar"));

        horizontalLayout->addWidget(btnCancelar);


        verticalLayout_3->addLayout(horizontalLayout);


        retranslateUi(NuevoVarios);

        QMetaObject::connectSlotsByName(NuevoVarios);
    } // setupUi

    void retranslateUi(QDialog *NuevoVarios)
    {
        NuevoVarios->setWindowTitle(QCoreApplication::translate("NuevoVarios", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("NuevoVarios", "Cliente / unidad minera", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("NuevoVarios", "Procedencia:", nullptr));
        btnAceptar->setText(QCoreApplication::translate("NuevoVarios", "Aceptar", nullptr));
        btnCancelar->setText(QCoreApplication::translate("NuevoVarios", "Cancelar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NuevoVarios: public Ui_NuevoVarios {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NUEVOVARIOS_H
