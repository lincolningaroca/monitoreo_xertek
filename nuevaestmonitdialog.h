#pragma once

#include <QDialog>
#include <QFile>
#include "bussineslayer.h"

namespace Ui {
  class NuevaEstMonitDialog;
}

class NuevaEstMonitDialog : public QDialog
{
  Q_OBJECT

public:
  explicit NuevaEstMonitDialog(QWidget *parent = nullptr);
  ~NuevaEstMonitDialog();
  void loadGpoMinerolist() noexcept;
  void loadDataListCliente() noexcept;
  void setUpToolBtnClear() noexcept;
  void loadDataProcedencia() noexcept;
  void loadDataReferencia() noexcept;

private slots:
  void on_btnGuardar_clicked();
  void on_btnFoto1_clicked();
  void on_btnCancelar_clicked();
  void on_btnFoto2_clicked();
  void on_btnFoto3_clicked();
  void on_txtPath1_textChanged(const QString &arg1);
  void on_txtPath2_textChanged(const QString &arg1);
  void on_pushButton_clicked();
  void on_btnProcedencia_clicked();
  void on_btnReferencia_clicked();
  void on_lwProcedencia_itemSelectionChanged();
  void on_cboUnidad_activated(int index);
  void on_cboGrupo_activated(int index);
  void on_btnNuevoGrupo_clicked();
  void on_btnNuevaUnnidad_clicked();
  void on_checkBox_toggled(bool checked);

private:
  Ui::NuevaEstMonitDialog *ui;
  BussinesLayer m_bLayer{};
//  QFile m_file{};
  QHash<unsigned int,QString> m_dataList{};
  QHash<unsigned int,QString> m_dataListCliente{};
  QHash<unsigned int,QString> m_dataProcedencia{};
  QHash<unsigned int,QString> m_dataReferencia{};

  QString m_oldPathimagen_1{""};
  QString m_oldPathimagen_2{""};
  QString m_oldPathimagen_3{""};

  QString m_pathImage_1{""};
  QString m_pathImage_2{""};
  QString m_pathImage_3{""};

  //  QVariant m_idEstacion{};

  QString m_desc_punto{""};
  QString m_pathToImage{""};

  //Metodos para caragar y escribir las configuraciones en el registro.
  void writeSettings() noexcept;
  void readSettings() noexcept;
  //generar menu contextual para agregar referencias
  void contextMenuGenRef();


  // QPaintDevice interface
public:
  virtual QPaintEngine *paintEngine() const override;
protected:
  virtual void closeEvent(QCloseEvent *event) override;
};


