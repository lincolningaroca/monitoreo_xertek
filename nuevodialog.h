#pragma once

#include "bussineslayer.h"
#include <QDialog>
//#include <QRegularExpression>
class QListWidgetItem;

namespace Ui {
class NuevoDialog;
}

class NuevoDialog : public QDialog
{
  Q_OBJECT

public:
  enum class DialogMode{
    GRUPO,
    UNIDAD,
    UPDATE_GPO,
    UPDATE_UNIDAD
  };

  explicit NuevoDialog(DialogMode mode, QVariantList data={}, QWidget *parent = nullptr);
  ~NuevoDialog();

  void loadDataList();
  void loadDataListCliente();

private slots:
  void on_btnGuardar_clicked();
  void on_btnCancelar_clicked();
  void on_cboLista_activated(int index);
  void on_lwUnidades_itemClicked(QListWidgetItem *item);

private:
  Ui::NuevoDialog *ui;
  BussinesLayer m_bussines{};
//  QRegularExpression regex;
  QHash<unsigned int,QString> m_dataList{};
  QHash<unsigned int,QString> m_dataListCliente{};

  const DialogMode m_mode{};
  const QVariantList m_data{};
  void setUpForm();

};


