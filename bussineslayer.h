#pragma once

#include <QVariant>
#include "typealiases.hpp"

class QSqlDatabase;
class QSqlQueryModel;

struct BussinesLayer {

public:
  enum class TypeStm { INSERT, UPDATE, DELETE };
  enum class Table { GPO_MINERO, CLIENTE, MONITOREO, CLIENTE_ALL };

  explicit BussinesLayer();
  ~BussinesLayer();

  bool gpoMineroAction(const QVariantList &, TypeStm);
  bool clienteMineroAction(const QVariantList &, TypeStm);
  bool monitoreoMineroAction(const QVariantList &, TypeStm);
  bool tablaMonitoreo(const QVariantList &, TypeStm);
  bool tablaReferencia(const QVariantList &, TypeStm);
  bool tablaReferenciaBatch(const QVariantList &, const QVariantList &, const QVariantList &,
                            const QVariantList &, const QVariantList &);
  bool tablaRefMonitoreo(const QVariantList &, TypeStm);
  bool updateReferencias(const QVariantList &);
  bool codeExists(QStringView, unsigned int);
  bool RefExists(QStringView, unsigned int);
  QString errorMessage() const noexcept{ return m_errorMessage; }
  QString errorCode() const noexcept { return m_errorCode; }
  const QVariant& lastInsertId() const noexcept{ return m_lInsertId; }
  QVariantList selectData(const QVariant &, Table);
  QVariantList dataEstMonitoreo(unsigned int, unsigned int);
  QStringList matrizList() noexcept;
  QStringList matrizDescList() noexcept;
  QStringList nComparacionList() noexcept;
  QHash<QString, QString> matriz() noexcept;
  QHash<unsigned int, QString> selectCodEstacion(unsigned int);
  QHash<unsigned int, QString> selectDataClient();
  QHash<unsigned int, QString> selectDataProc(unsigned int);
  //  QHash<int, QString> selectDataProc();
  QHash<unsigned int, QString> selectDataRef(unsigned int);
  QVariantList selectDataRef_All(unsigned int);
  QHash<unsigned int, QString> selectDataMonitoreo(unsigned int);
  QHash<unsigned int, QString> gpoMineroList(Table, unsigned int id = 0);
  QSqlQueryModel *selectCodEstacion_q(unsigned int) noexcept;
  bool createDirPictures() const noexcept;
//  QStringList completerList();
  QString relativePath() const noexcept;

private:
  QString m_errorMessage{""};
  QString m_errorCode{""};
  QVariant m_lInsertId{};
  const QSqlDatabase m_db{};
  SW::qry qry{};
  //  metodos privados
  void setError(const SW::qry& qry) noexcept;
};


